#include <gtest/gtest.h>

#include <algorithm>
#include "errormsg.hpp"

class ErrList : public GBDev::ErrorMsgMgr {
 public:
  std::string Store(const std::string& src, const int pos, const int len) {
    GBDev::TextCursor cursor;
    auto pos_itr{src.begin() + pos};
    cursor.pos = pos;
    cursor.len = len;
    cursor.row = std::count(src.begin(), pos_itr, '\n');
    cursor.col = pos % 16;
    errors_.emplace_back("test mssage", cursor);
    return src.substr(pos, len);
  }
};

class ErrorMsgTest : public ::testing::Test {
 protected:
  virtual void SetUp() {
    src.append(
      "this is line 01\n"
      "this is line 02\n"
      "this is line 03\n"
      "this is line 04\n"
      "this is line 05\n"
      "this is line 06\n"
      "this is line 07\n"
      "this is line 08\n"
      "this is line 09\n"
      "this is line 10\n");
  }
  std::string src;
};

TEST_F(ErrorMsgTest, FirstLine) {
  ErrList errlist;
  auto word{errlist.Store(src, 0, 4)};
  std::cout << "select word -> " << word << "\n";
  auto err{errlist.GetErrors().begin()};
  GBDev::ErrorMsgHelper::Output(*err, src, &std::cout);
  ASSERT_EQ("this", word);
  ASSERT_EQ(0, err->cursor.row);
  ASSERT_EQ(0, err->cursor.col);
  ASSERT_EQ(4, err->cursor.len);
}

TEST_F(ErrorMsgTest, MiddleLine) {
  ErrList errlist;
  auto word{errlist.Store(src, 69, 2)};
  std::cout << "select word -> " << word << "\n";
  auto err{errlist.GetErrors().begin()};
  GBDev::ErrorMsgHelper::Output(*err, src, &std::cout);
  ASSERT_EQ("is", word);
  ASSERT_EQ(4, err->cursor.row);
  ASSERT_EQ(5, err->cursor.col);
  ASSERT_EQ(2, err->cursor.len);
}

TEST_F(ErrorMsgTest, LastLine) {
  ErrList errlist;
  auto word{errlist.Store(src, 152, 7)};
  std::cout << "select word -> " << word << "\n";
  auto err{errlist.GetErrors().begin()};
  GBDev::ErrorMsgHelper::Output(*err, src, &std::cout);
  ASSERT_EQ("line 10", word);
  ASSERT_EQ(9, err->cursor.row);
  ASSERT_EQ(8, err->cursor.col);
  ASSERT_EQ(7, err->cursor.len);
}

TEST_F(ErrorMsgTest, MargeTest) {
  ErrList errlist1;
  auto word1{errlist1.Store(src, 0, 4)};
  ErrList errlist2;
  auto word2{errlist2.Store(src, 69, 2)};
  ErrList errlist3;
  auto word3{errlist3.Store(src, 152, 7)};
  errlist1.MergeErrors(errlist2);
  errlist1.MergeErrors(errlist3);
  const auto err{errlist1.GetErrors()};
  auto err_itr{err.begin()};
  ASSERT_EQ("this", word1);
  ASSERT_EQ(0, err_itr->cursor.row);
  ASSERT_EQ(0, err_itr->cursor.col);
  ASSERT_EQ(4, err_itr->cursor.len);
  ++err_itr;
  ASSERT_EQ("is", word2);
  ASSERT_EQ(4, err_itr->cursor.row);
  ASSERT_EQ(5, err_itr->cursor.col);
  ASSERT_EQ(2, err_itr->cursor.len);
  ++err_itr;
  ASSERT_EQ("line 10", word3);
  ASSERT_EQ(9, err_itr->cursor.row);
  ASSERT_EQ(8, err_itr->cursor.col);
  ASSERT_EQ(7, err_itr->cursor.len);
  for (const auto& item : err) {
    GBDev::ErrorMsgHelper::Output(item, src, &std::cout);
  }
}