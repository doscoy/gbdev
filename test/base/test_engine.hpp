#pragma once

#include <deque>
#include <sstream>

#include "errormsg.hpp"
#include "generator.hpp"
#include "parser.hpp"
#include "tokenizer.hpp"

class TestTokenizerImpl {
  enum class Kind { UNKNOW, DATA, DELIM };
  using Base = GBDev::TokenizerBase<TestTokenizerImpl>;
  friend Base;

  using Token = typename GBDev::Token<Kind>;

  TestTokenizerImpl(Base& base) : base_(base) {}

  void Tokenize(const std::string& src) {
    const auto beg{src.cbegin()};
    const auto end{src.cend()};
    GBDev::TextCursor cursor{0, 0, 1, 0, 0};
    for (auto itr = beg; itr < end; ++itr, ++cursor.pos, ++cursor.col) {
      if (*itr == '!') {
        base_.errors_.emplace_back("error: \"!\" is invalid", cursor);
        continue;
      }
      if (*itr == '\n') {
        ++cursor.row;
        cursor.col = -1;
        continue;
      }
      const auto kind{*itr == ',' ? Token::Kind::DELIM : Token::Kind::DATA};
      std::string tmp{*itr};
      base_.token_.emplace_back(kind, std::move(tmp), cursor);
    }
  }
  Base& base_;
};
using TestTokenizer = GBDev::TokenizerBase<TestTokenizerImpl>;

// expr = data ("," data)*
class TestParserImpl {
  enum class Kind { UNKNOW, DATA, DELIM };
  using Tokenizer = TestTokenizer;
  using Base = GBDev::ParserBase<TestParserImpl>;
  friend Base;

  using Token = typename Tokenizer::Token;
  using TokenContainer = typename Tokenizer::TokenContainer;
  using TokenItr = TokenContainer::const_iterator;
  using Node = typename GBDev::Node<Kind, Token>;
  using NodePtr = typename Node::Pointer;

  TestParserImpl(Base& base) : base_(base) {}

  void Parse(const TokenContainer& token) {
    auto itr{token.begin()};
    const auto end{token.end()};
    while (itr < end) {
      base_.nodes_.emplace_back(Expr(itr));
    }
  }

  NodePtr Expr(TokenItr& itr) {
    auto node{Data(itr)};
    while (true) {
      if (itr->kind == Token::Kind::DELIM) {
        auto token{*itr};
        node = Node::MakePtr(Node::Kind::DELIM, std::move(node), Data(++itr),
                             std::move(token));
      } else {
        return node;
      }
    }
    return node;
  }

  NodePtr Data(TokenItr& itr) {
    if (itr->kind == Token::Kind::DATA) {
      int value{static_cast<int>(*(itr->str.begin()))};
      auto token{*itr};
      if (token.str.find('x') != decltype(token.str)::npos) {
        base_.errors_.emplace_back("error: \"x\" is invalid", token.cursor);
      }
      ++itr;
      return Node::MakePtr(Node::Kind::DATA, std::move(token), value);
    } else {
      auto token{*itr};
      ++itr;
      return Node::MakePtr(Node::Kind::DATA, std::move(token), -1);
    }
  }

  Base& base_;
};
using TestParser = GBDev::ParserBase<TestParserImpl>;

class TestGeneratorImpl {
  struct Data {
    std::stringstream os;
  };
  using Parser = TestParser;
  using Base = GBDev::GeneratorBase<TestGeneratorImpl>;
  friend Base;

  using Node = typename TestParser::Node;
  using NodePtr = typename Node::Pointer;

  TestGeneratorImpl(Base& base) : base_(base) {}

  void Generate(const Parser::NodeContainer& nodes) {
    for (const auto& node : nodes) {
      GenerateImpl(node);
    }
  }
  
  void GenerateImpl(const NodePtr& node) {
    if (node == nullptr) return;
    switch (node->kind) {
      case Node::Kind::DATA:
        base_.data_.os << node->value;
        if (node->value >= 100) {
          base_.errors_.emplace_back("error: value is 100 or more",
                                     node->token.cursor);
        }
        break;
      case Node::Kind::DELIM:
        GenerateImpl(node->lhs);
        base_.data_.os << ' ';
        break;
      default:
        break;
    }
    GenerateImpl(node->rhs);
  }

  Base& base_;
};
using TestGenerator = GBDev::GeneratorBase<TestGeneratorImpl>;