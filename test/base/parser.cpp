#include "parser.hpp"
#include "test_engine.hpp"
#include "errormsg.hpp"

#include <gtest/gtest.h>

namespace {
using TT = TestTokenizer;
using TP = TestParser;
}

TEST(NodeTest, IsEmpty) {
  TP::NodePtr test{TP::Node::MakePtr(TP::Node::Kind::DATA)};
  ASSERT_EQ(test->lhs, nullptr);
  ASSERT_EQ(test->rhs, nullptr);
}

TEST(NodeTest, AddNewNode) {
  auto leaf1{TP::Node::MakePtr(TP::Node::Kind::DATA, 1)};
  auto leaf2{TP::Node::MakePtr(TP::Node::Kind::DATA, 2)};
  auto leaf3{TP::Node::MakePtr(TP::Node::Kind::DATA, 3)};
  auto node{TP::Node::MakePtr(TP::Node::Kind::DELIM, std::move(leaf2), std::move(leaf3))};
  auto root{TP::Node::MakePtr(TP::Node::Kind::DELIM, std::move(leaf1), std::move(node))};
  ASSERT_EQ(root->kind, TP::Node::Kind::DELIM);
  ASSERT_EQ(root->lhs, leaf1);
  ASSERT_EQ(root->lhs->kind, TP::Node::Kind::DATA);
  ASSERT_EQ(root->lhs->value, 1);
  ASSERT_EQ(root->rhs, node);
  ASSERT_EQ(root->rhs->kind, TP::Node::Kind::DELIM);
  ASSERT_EQ(root->rhs->lhs, leaf2);
  ASSERT_EQ(root->rhs->lhs->kind, TP::Node::Kind::DATA);
  ASSERT_EQ(root->rhs->lhs->value, 2);
  ASSERT_EQ(root->rhs->rhs, leaf3);
  ASSERT_EQ(root->rhs->rhs->kind, TP::Node::Kind::DATA);
  ASSERT_EQ(root->rhs->rhs->value, 3);
}

TEST(ParserTest, NodeCheck) {
  std::string src{"a,b,c"};
  TT tokenizer{src};
  TP parser{tokenizer.GetToken()};
  const auto nodes{parser.GetNodes()};
  ASSERT_EQ(nodes.size(), 1);
  const auto cur{nodes[0]};
  ASSERT_EQ(cur->kind, TP::Node::Kind::DELIM);
  ASSERT_EQ(cur->rhs->kind, TP::Node::Kind::DATA);
  ASSERT_EQ(cur->rhs->value, 'c');
  ASSERT_EQ(cur->lhs->kind, TP::Node::Kind::DELIM);
  ASSERT_EQ(cur->lhs->rhs->kind, TP::Node::Kind::DATA);
  ASSERT_EQ(cur->lhs->rhs->value, 'b');
  ASSERT_EQ(cur->lhs->lhs->kind, TP::Node::Kind::DATA);
  ASSERT_EQ(cur->lhs->lhs->value, 'a');
}

TEST(ParserTest, ErrorCheck) {
  std::string src{"a,x,c\na,b,x"};
  TT tokenizer{src};
  TP parser{tokenizer.GetToken()};
  const auto nodes{parser.GetNodes()};
  const auto err{parser.GetErrors()};
  auto err_itr{err.begin()};
  ASSERT_EQ(parser.GetErrorCount(), 2);
  ASSERT_EQ(err_itr->cursor.row, 0);
  ASSERT_EQ(err_itr->cursor.col, 2);
  ASSERT_EQ(err_itr->cursor.len, 1);
  ++err_itr;
  ASSERT_EQ(err_itr->cursor.row, 1);
  ASSERT_EQ(err_itr->cursor.col, 4);
  ASSERT_EQ(err_itr->cursor.len, 1);
  for(const auto& item : err) {
    GBDev::ErrorMsgHelper::Output(item, src, &std::cout);
  }
}