#include "tokenizer.hpp"

#include <gtest/gtest.h>

#include "errormsg.hpp"
#include "test_engine.hpp"

namespace {
using TT = TestTokenizer;
}

TEST(TokenizerTest, TokenCheck) {
  TT test;
  test.SetSource("a,b,c");
  auto token{test.GetToken()};
  ASSERT_EQ(token[0].str, "a");
  ASSERT_EQ(token[0].kind, TT::Token::Kind::DATA);
  ASSERT_EQ(token[1].str, ",");
  ASSERT_EQ(token[1].kind, TT::Token::Kind::DELIM);
  ASSERT_EQ(token[2].str, "b");
  ASSERT_EQ(token[2].kind, TT::Token::Kind::DATA);
  ASSERT_EQ(token[3].str, ",");
  ASSERT_EQ(token[3].kind, TT::Token::Kind::DELIM);
  ASSERT_EQ(token[4].str, "c");
  ASSERT_EQ(token[4].kind, TT::Token::Kind::DATA);
}

TEST(TokenizerTest, ErrorCheck) {
  TT test;
  std::string src{"a!b!c"};
  test.SetSource(src);
  auto token{test.GetToken()};
  ASSERT_EQ(token[0].str, "a");
  ASSERT_EQ(token[0].kind, TT::Token::Kind::DATA);
  ASSERT_EQ(token[1].str, "b");
  ASSERT_EQ(token[1].kind, TT::Token::Kind::DATA);
  ASSERT_EQ(token[2].str, "c");
  ASSERT_EQ(token[2].kind, TT::Token::Kind::DATA);
  ASSERT_EQ(test.GetErrorCount(), 2);
  const auto err{test.GetErrors()};
  auto err_itr{err.begin()};
  ASSERT_EQ(err_itr->cursor.row, 0);
  ASSERT_EQ(err_itr->cursor.col, 1);
  ASSERT_EQ(err_itr->cursor.len, 1);
  ++err_itr;
  ASSERT_EQ(err_itr->cursor.row, 0);
  ASSERT_EQ(err_itr->cursor.col, 3);
  ASSERT_EQ(err_itr->cursor.len, 1);
  for (const auto& item : err) {
    GBDev::ErrorMsgHelper::Output(item, src, &std::cout);
  }
}