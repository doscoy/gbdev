#include "test_engine.hpp"
#include "generator.hpp"

#include <sstream>

#include <gtest/gtest.h>

namespace {
using TT = TestTokenizer;
using TP = TestParser;
using TG = TestGenerator;
}

TEST(GenTest, OutputCheck) {
  std::string src{"a,b,c"};
  TT tokenizer{src};
  TP parser{tokenizer.GetToken()};
  TG generator{parser.GetNodes()};
  const TG::Data& data{generator.GetData()};
  ASSERT_EQ(data.os.str(), "97 98 99");
}

TEST(GenTest, ErrorCheck) {
  std::string src{"a,b,c,y\na,b,c,z"};
  TT tokenizer{src};
  TP parser{tokenizer.GetToken()};
  TG generator{parser.GetNodes()};
  [[maybe_unused]]const TG::Data& data{generator.GetData()};
  const auto err{generator.GetErrors()};
  auto err_itr{err.begin()};
  ASSERT_EQ(generator.GetErrorCount(), 2);
  ASSERT_EQ(err_itr->cursor.row, 0);
  ASSERT_EQ(err_itr->cursor.col, 6);
  ASSERT_EQ(err_itr->cursor.len, 1);
  ++err_itr;
  ASSERT_EQ(err_itr->cursor.row, 1);
  ASSERT_EQ(err_itr->cursor.col, 6);
  ASSERT_EQ(err_itr->cursor.len, 1);
  for(const auto& item : err) {
    GBDev::ErrorMsgHelper::Output(item, src, &std::cout);
  }
}