#include <gtest/gtest.h>

#include <fstream>
#include <sstream>

#include "generator_impl.hpp"

class MnemonicTest : public ::testing::TestWithParam<
                         std::pair<std::string, std::vector<unsigned char>>> {
 protected:
  void SetUp() override { std::tie(source_, answer_) = GetParam(); }
  std::string source_;
  std::vector<unsigned char> answer_;
};

TEST_P(MnemonicTest, TranslateCheck) {
  GBDev::AssemblerImpl::Tokenizer tokenizer{source_};
  GBDev::AssemblerImpl::Parser parser{tokenizer.GetToken()};
  GBDev::AssemblerImpl::Generator generator{parser.GetNodes()};
  auto& data{generator.GetData()};
  auto bin{data.os.rawbuf()};
  auto counter{0};
  if (bin.size() != answer_.size()) {
    std::cout << "size error\n";
    FAIL();
  }
  for (const auto& c : bin) {
    std::cout << std::hex << std::uppercase << std::setw(2) << std::setfill('0')
              << (int)(unsigned char)c << " ";
    ASSERT_EQ(answer_[counter], (unsigned char)c);
    ++counter;
  }
  std::cout << "\n";
}

const std::pair<std::string, std::vector<unsigned char>> kTestCodes[]{
    {"code0x00:   NOP                  \n", {0x00}},
    {"code0x01:   LD       BC,   0xFFFF\n", {0x01, 0xFF, 0xFF}},
    {"code0x02:   LD     [BC],        A\n", {0x02}},
    {"code0x03:   INC      BC          \n", {0x03}},
    {"code0x04:   INC       B          \n", {0x04}},
    {"code0x05:   DEC       B          \n", {0x05}},
    {"code0x06:   LD        B,     0xFF\n", {0x06, 0xFF}},
    {"code0x07:   RLCA                 \n", {0x07}},
    {"code0x08:   LD [0xFFFF],       SP\n", {0x08, 0xFF, 0xFF}},
    {"code0x09:   ADD      HL,       BC\n", {0x09}},
    {"code0x0A:   LD        A,     [BC]\n", {0x0A}},
    {"code0x0B:   DEC      BC          \n", {0x0B}},
    {"code0x0C:   INC       C          \n", {0x0C}},
    {"code0x0D:   DEC       C          \n", {0x0D}},
    {"code0x0E:   LD        C,     0xFF\n", {0x0E, 0xFF}},
    {"code0x0F:   RRCA                 \n", {0x0F}},
    {"code0x10:   STOP                 \n", {0x10, 0x00}},
    {"code0x11:   LD       DE,   0xFFFF\n", {0x11, 0xFF, 0xFF}},
    {"code0x12:   LD     [DE],        A\n", {0x12}},
    {"code0x13:   INC      DE          \n", {0x13}},
    {"code0x14:   INC       D          \n", {0x14}},
    {"code0x15:   DEC       D          \n", {0x15}},
    {"code0x16:   LD        D,     0xFF\n", {0x16, 0xFF}},
    {"code0x17:   RLA                  \n", {0x17}},
    {"code0x18:   JR               0xFF\n", {0x18, 0xFF}},
    {"code0x19:   ADD      HL,       DE\n", {0x19}},
    {"code0x1A:   LD        A,     [DE]\n", {0x1A}},
    {"code0x1B:   DEC      DE          \n", {0x1B}},
    {"code0x1C:   INC       E          \n", {0x1C}},
    {"code0x1D:   DEC       E          \n", {0x1D}},
    {"code0x1E:   LD        E,     0xFF\n", {0x1E, 0xFF}},
    {"code0x1F:   RRA                  \n", {0x1F}},
    {"code0x20:   JR       NZ,     0xFF\n", {0x20, 0xFF}},
    {"code0x21:   LD       HL,   0xFFFF\n", {0x21, 0xFF, 0xFF}},
    {"code0x22:   LDI    [HL],        A\n", {0x22}},
    {"code0x23:   INC      HL          \n", {0x23}},
    {"code0x24:   INC       H          \n", {0x24}},
    {"code0x25:   DEC       H          \n", {0x25}},
    {"code0x26:   LD        H,     0xFF\n", {0x26, 0xFF}},
    {"code0x27:   DAA                  \n", {0x27}},
    {"code0x28:   JR        Z,     0xFF\n", {0x28, 0xFF}},
    {"code0x29:   ADD      HL,       HL\n", {0x29}},
    {"code0x2A:   LDI       A,     [HL]\n", {0x2A}},
    {"code0x2B:   DEC      HL          \n", {0x2B}},
    {"code0x2C:   INC       L          \n", {0x2C}},
    {"code0x2D:   DEC       L          \n", {0x2D}},
    {"code0x2E:   LD        L,     0xFF\n", {0x2E, 0xFF}},
    {"code0x2F:   CPL                  \n", {0x2F}},
    {"code0x30:   JR       NC,     0xFF\n", {0x30, 0xFF}},
    {"code0x31:   LD       SP,   0xFFFF\n", {0x31, 0xFF, 0xFF}},
    {"code0x32:   LDD    [HL],        A\n", {0x32}},
    {"code0x33:   INC      SP          \n", {0x33}},
    {"code0x34:   INC    [HL]          \n", {0x34}},
    {"code0x35:   DEC    [HL]          \n", {0x35}},
    {"code0x36:   LD     [HL],     0xFF\n", {0x36, 0xFF}},
    {"code0x37:   SCF                  \n", {0x37}},
    {"code0x38:   JR        C,     0xFF\n", {0x38, 0xFF}},
    {"code0x39:   ADD      HL,       SP\n", {0x39}},
    {"code0x3A:   LDD       A,     [HL]\n", {0x3A}},
    {"code0x3B:   DEC      SP          \n", {0x3B}},
    {"code0x3C:   INC       A          \n", {0x3C}},
    {"code0x3D:   DEC       A          \n", {0x3D}},
    {"code0x3E:   LD        A,     0xFF\n", {0x3E, 0xFF}},
    {"code0x3F:   CCF                  \n", {0x3F}},
    {"code0x40:   LD        B,        B\n", {0x40}},
    {"code0x41:   LD        B,        C\n", {0x41}},
    {"code0x42:   LD        B,        D\n", {0x42}},
    {"code0x43:   LD        B,        E\n", {0x43}},
    {"code0x44:   LD        B,        H\n", {0x44}},
    {"code0x45:   LD        B,        L\n", {0x45}},
    {"code0x46:   LD        B,     [HL]\n", {0x46}},
    {"code0x47:   LD        B,        A\n", {0x47}},
    {"code0x48:   LD        C,        B\n", {0x48}},
    {"code0x49:   LD        C,        C\n", {0x49}},
    {"code0x4A:   LD        C,        D\n", {0x4A}},
    {"code0x4B:   LD        C,        E\n", {0x4B}},
    {"code0x4C:   LD        C,        H\n", {0x4C}},
    {"code0x4D:   LD        C,        L\n", {0x4D}},
    {"code0x4E:   LD        C,     [HL]\n", {0x4E}},
    {"code0x4F:   LD        C,        A\n", {0x4F}},
    {"code0x50:   LD        D,        B\n", {0x50}},
    {"code0x51:   LD        D,        C\n", {0x51}},
    {"code0x52:   LD        D,        D\n", {0x52}},
    {"code0x53:   LD        D,        E\n", {0x53}},
    {"code0x54:   LD        D,        H\n", {0x54}},
    {"code0x55:   LD        D,        L\n", {0x55}},
    {"code0x56:   LD        D,     [HL]\n", {0x56}},
    {"code0x57:   LD        D,        A\n", {0x57}},
    {"code0x58:   LD        E,        B\n", {0x58}},
    {"code0x59:   LD        E,        C\n", {0x59}},
    {"code0x5A:   LD        E,        D\n", {0x5A}},
    {"code0x5B:   LD        E,        E\n", {0x5B}},
    {"code0x5C:   LD        E,        H\n", {0x5C}},
    {"code0x5D:   LD        E,        L\n", {0x5D}},
    {"code0x5E:   LD        E,     [HL]\n", {0x5E}},
    {"code0x5F:   LD        E,        A\n", {0x5F}},
    {"code0x60:   LD        H,        B\n", {0x60}},
    {"code0x61:   LD        H,        C\n", {0x61}},
    {"code0x62:   LD        H,        D\n", {0x62}},
    {"code0x63:   LD        H,        E\n", {0x63}},
    {"code0x64:   LD        H,        H\n", {0x64}},
    {"code0x65:   LD        H,        L\n", {0x65}},
    {"code0x66:   LD        H,     [HL]\n", {0x66}},
    {"code0x67:   LD        H,        A\n", {0x67}},
    {"code0x68:   LD        L,        B\n", {0x68}},
    {"code0x69:   LD        L,        C\n", {0x69}},
    {"code0x6A:   LD        L,        D\n", {0x6A}},
    {"code0x6B:   LD        L,        E\n", {0x6B}},
    {"code0x6C:   LD        L,        H\n", {0x6C}},
    {"code0x6D:   LD        L,        L\n", {0x6D}},
    {"code0x6E:   LD        L,     [HL]\n", {0x6E}},
    {"code0x6F:   LD        L,        A\n", {0x6F}},
    {"code0x70:   LD     [HL],        B\n", {0x70}},
    {"code0x71:   LD     [HL],        C\n", {0x71}},
    {"code0x72:   LD     [HL],        D\n", {0x72}},
    {"code0x73:   LD     [HL],        E\n", {0x73}},
    {"code0x74:   LD     [HL],        H\n", {0x74}},
    {"code0x75:   LD     [HL],        L\n", {0x75}},
    {"code0x76:   HALT                 \n", {0x76}},
    {"code0x77:   LD     [HL],        A\n", {0x77}},
    {"code0x78:   LD        A,        B\n", {0x78}},
    {"code0x79:   LD        A,        C\n", {0x79}},
    {"code0x7A:   LD        A,        D\n", {0x7A}},
    {"code0x7B:   LD        A,        E\n", {0x7B}},
    {"code0x7C:   LD        A,        H\n", {0x7C}},
    {"code0x7D:   LD        A,        L\n", {0x7D}},
    {"code0x7E:   LD        A,     [HL]\n", {0x7E}},
    {"code0x7F:   LD        A,        A\n", {0x7F}},
    {"code0x80:   ADD       A,        B\n", {0x80}},
    {"code0x81:   ADD       A,        C\n", {0x81}},
    {"code0x82:   ADD       A,        D\n", {0x82}},
    {"code0x83:   ADD       A,        E\n", {0x83}},
    {"code0x84:   ADD       A,        H\n", {0x84}},
    {"code0x85:   ADD       A,        L\n", {0x85}},
    {"code0x86:   ADD       A,     [HL]\n", {0x86}},
    {"code0x87:   ADD       A,        A\n", {0x87}},
    {"code0x88:   ADC       A,        B\n", {0x88}},
    {"code0x89:   ADC       A,        C\n", {0x89}},
    {"code0x8A:   ADC       A,        D\n", {0x8A}},
    {"code0x8B:   ADC       A,        E\n", {0x8B}},
    {"code0x8C:   ADC       A,        H\n", {0x8C}},
    {"code0x8D:   ADC       A,        L\n", {0x8D}},
    {"code0x8E:   ADC       A,     [HL]\n", {0x8E}},
    {"code0x8F:   ADC       A,        A\n", {0x8F}},
    {"code0x90:   SUB       B          \n", {0x90}},
    {"code0x91:   SUB       C          \n", {0x91}},
    {"code0x92:   SUB       D          \n", {0x92}},
    {"code0x93:   SUB       E          \n", {0x93}},
    {"code0x94:   SUB       H          \n", {0x94}},
    {"code0x95:   SUB       L          \n", {0x95}},
    {"code0x96:   SUB    [HL]          \n", {0x96}},
    {"code0x97:   SUB       A          \n", {0x97}},
    {"code0x98:   SBC       A,        B\n", {0x98}},
    {"code0x99:   SBC       A,        C\n", {0x99}},
    {"code0x9A:   SBC       A,        D\n", {0x9A}},
    {"code0x9B:   SBC       A,        E\n", {0x9B}},
    {"code0x9C:   SBC       A,        H\n", {0x9C}},
    {"code0x9D:   SBC       A,        L\n", {0x9D}},
    {"code0x9E:   SBC       A,     [HL]\n", {0x9E}},
    {"code0x9F:   SBC       A,        A\n", {0x9F}},
    {"code0xA0:   AND       B          \n", {0xA0}},
    {"code0xA1:   AND       C          \n", {0xA1}},
    {"code0xA2:   AND       D          \n", {0xA2}},
    {"code0xA3:   AND       E          \n", {0xA3}},
    {"code0xA4:   AND       H          \n", {0xA4}},
    {"code0xA5:   AND       L          \n", {0xA5}},
    {"code0xA6:   AND    [HL]          \n", {0xA6}},
    {"code0xA7:   AND       A          \n", {0xA7}},
    {"code0xA8:   XOR       B          \n", {0xA8}},
    {"code0xA9:   XOR       C          \n", {0xA9}},
    {"code0xAA:   XOR       D          \n", {0xAA}},
    {"code0xAB:   XOR       E          \n", {0xAB}},
    {"code0xAC:   XOR       H          \n", {0xAC}},
    {"code0xAD:   XOR       L          \n", {0xAD}},
    {"code0xAE:   XOR    [HL]          \n", {0xAE}},
    {"code0xAF:   XOR       A          \n", {0xAF}},
    {"code0xB0:   OR        B          \n", {0xB0}},
    {"code0xB1:   OR        C          \n", {0xB1}},
    {"code0xB2:   OR        D          \n", {0xB2}},
    {"code0xB3:   OR        E          \n", {0xB3}},
    {"code0xB4:   OR        H          \n", {0xB4}},
    {"code0xB5:   OR        L          \n", {0xB5}},
    {"code0xB6:   OR     [HL]          \n", {0xB6}},
    {"code0xB7:   OR        A          \n", {0xB7}},
    {"code0xB8:   CP        B          \n", {0xB8}},
    {"code0xB9:   CP        C          \n", {0xB9}},
    {"code0xBA:   CP        D          \n", {0xBA}},
    {"code0xBB:   CP        E          \n", {0xBB}},
    {"code0xBC:   CP        H          \n", {0xBC}},
    {"code0xBD:   CP        L          \n", {0xBD}},
    {"code0xBE:   CP     [HL]          \n", {0xBE}},
    {"code0xBF:   CP        A          \n", {0xBF}},
    {"code0xC0:   RET      NZ          \n", {0xC0}},
    {"code0xC1:   POP      BC          \n", {0xC1}},
    {"code0xC2:   JP       NZ,   0xFFFF\n", {0xC2, 0xFF, 0xFF}},
    {"code0xC3:   JP             0xFFFF\n", {0xC3, 0xFF, 0xFF}},
    {"code0xC4:   CALL     NZ,   0xFFFF\n", {0xC4, 0xFF, 0xFF}},
    {"code0xC5:   PUSH     BC          \n", {0xC5}},
    {"code0xC6:   ADD       A,     0xFF\n", {0xC6, 0xFF}},
    {"code0xC7:   RST              0x00\n", {0xC7}},
    {"code0xC8:   RET       Z          \n", {0xC8}},
    {"code0xC9:   RET                  \n", {0xC9}},
    {"code0xCA:   JP        Z,   0xFFFF\n", {0xCA, 0xFF, 0xFF}},
    {"code0xCB00: RLC       B          \n", {0xCB, 0x00}},
    {"code0xCB01: RLC       C          \n", {0xCB, 0x01}},
    {"code0xCB02: RLC       D          \n", {0xCB, 0x02}},
    {"code0xCB03: RLC       E          \n", {0xCB, 0x03}},
    {"code0xCB04: RLC       H          \n", {0xCB, 0x04}},
    {"code0xCB05: RLC       L          \n", {0xCB, 0x05}},
    {"code0xCB06: RLC    [HL]          \n", {0xCB, 0x06}},
    {"code0xCB07: RLC       A          \n", {0xCB, 0x07}},
    {"code0xCB08: RRC       B          \n", {0xCB, 0x08}},
    {"code0xCB09: RRC       C          \n", {0xCB, 0x09}},
    {"code0xCB0A: RRC       D          \n", {0xCB, 0x0A}},
    {"code0xCB0B: RRC       E          \n", {0xCB, 0x0B}},
    {"code0xCB0C: RRC       H          \n", {0xCB, 0x0C}},
    {"code0xCB0D: RRC       L          \n", {0xCB, 0x0D}},
    {"code0xCB0E: RRC    [HL]          \n", {0xCB, 0x0E}},
    {"code0xCB0F: RRC       A          \n", {0xCB, 0x0F}},
    {"code0xCB10: RL        B          \n", {0xCB, 0x10}},
    {"code0xCB11: RL        C          \n", {0xCB, 0x11}},
    {"code0xCB12: RL        D          \n", {0xCB, 0x12}},
    {"code0xCB13: RL        E          \n", {0xCB, 0x13}},
    {"code0xCB14: RL        H          \n", {0xCB, 0x14}},
    {"code0xCB15: RL        L          \n", {0xCB, 0x15}},
    {"code0xCB16: RL     [HL]          \n", {0xCB, 0x16}},
    {"code0xCB17: RL        A          \n", {0xCB, 0x17}},
    {"code0xCB18: RR        B          \n", {0xCB, 0x18}},
    {"code0xCB19: RR        C          \n", {0xCB, 0x19}},
    {"code0xCB1A: RR        D          \n", {0xCB, 0x1A}},
    {"code0xCB1B: RR        E          \n", {0xCB, 0x1B}},
    {"code0xCB1C: RR        H          \n", {0xCB, 0x1C}},
    {"code0xCB1D: RR        L          \n", {0xCB, 0x1D}},
    {"code0xCB1E: RR     [HL]          \n", {0xCB, 0x1E}},
    {"code0xCB1F: RR        A          \n", {0xCB, 0x1F}},
    {"code0xCB20: SLA       B          \n", {0xCB, 0x20}},
    {"code0xCB21: SLA       C          \n", {0xCB, 0x21}},
    {"code0xCB22: SLA       D          \n", {0xCB, 0x22}},
    {"code0xCB23: SLA       E          \n", {0xCB, 0x23}},
    {"code0xCB24: SLA       H          \n", {0xCB, 0x24}},
    {"code0xCB25: SLA       L          \n", {0xCB, 0x25}},
    {"code0xCB26: SLA    [HL]          \n", {0xCB, 0x26}},
    {"code0xCB27: SLA       A          \n", {0xCB, 0x27}},
    {"code0xCB28: SRA       B          \n", {0xCB, 0x28}},
    {"code0xCB29: SRA       C          \n", {0xCB, 0x29}},
    {"code0xCB2A: SRA       D          \n", {0xCB, 0x2A}},
    {"code0xCB2B: SRA       E          \n", {0xCB, 0x2B}},
    {"code0xCB2C: SRA       H          \n", {0xCB, 0x2C}},
    {"code0xCB2D: SRA       L          \n", {0xCB, 0x2D}},
    {"code0xCB2E: SRA    [HL]          \n", {0xCB, 0x2E}},
    {"code0xCB2F: SRA       A          \n", {0xCB, 0x2F}},
    {"code0xCB30: SWAP      B          \n", {0xCB, 0x30}},
    {"code0xCB31: SWAP      C          \n", {0xCB, 0x31}},
    {"code0xCB32: SWAP      D          \n", {0xCB, 0x32}},
    {"code0xCB33: SWAP      E          \n", {0xCB, 0x33}},
    {"code0xCB34: SWAP      H          \n", {0xCB, 0x34}},
    {"code0xCB35: SWAP      L          \n", {0xCB, 0x35}},
    {"code0xCB36: SWAP   [HL]          \n", {0xCB, 0x36}},
    {"code0xCB37: SWAP      A          \n", {0xCB, 0x37}},
    {"code0xCB38: SRL       B          \n", {0xCB, 0x38}},
    {"code0xCB39: SRL       C          \n", {0xCB, 0x39}},
    {"code0xCB3A: SRL       D          \n", {0xCB, 0x3A}},
    {"code0xCB3B: SRL       E          \n", {0xCB, 0x3B}},
    {"code0xCB3C: SRL       H          \n", {0xCB, 0x3C}},
    {"code0xCB3D: SRL       L          \n", {0xCB, 0x3D}},
    {"code0xCB3E: SRL    [HL]          \n", {0xCB, 0x3E}},
    {"code0xCB3F: SRL       A          \n", {0xCB, 0x3F}},
    {"code0xCB40: BIT       0,        B\n", {0xCB, 0x40}},
    {"code0xCB41: BIT       0,        C\n", {0xCB, 0x41}},
    {"code0xCB42: BIT       0,        D\n", {0xCB, 0x42}},
    {"code0xCB43: BIT       0,        E\n", {0xCB, 0x43}},
    {"code0xCB44: BIT       0,        H\n", {0xCB, 0x44}},
    {"code0xCB45: BIT       0,        L\n", {0xCB, 0x45}},
    {"code0xCB46: BIT       0,     [HL]\n", {0xCB, 0x46}},
    {"code0xCB47: BIT       0,        A\n", {0xCB, 0x47}},
    {"code0xCB48: BIT       1,        B\n", {0xCB, 0x48}},
    {"code0xCB49: BIT       1,        C\n", {0xCB, 0x49}},
    {"code0xCB4A: BIT       1,        D\n", {0xCB, 0x4A}},
    {"code0xCB4B: BIT       1,        E\n", {0xCB, 0x4B}},
    {"code0xCB4C: BIT       1,        H\n", {0xCB, 0x4C}},
    {"code0xCB4D: BIT       1,        L\n", {0xCB, 0x4D}},
    {"code0xCB4E: BIT       1,     [HL]\n", {0xCB, 0x4E}},
    {"code0xCB4F: BIT       1,        A\n", {0xCB, 0x4F}},
    {"code0xCB50: BIT       2,        B\n", {0xCB, 0x50}},
    {"code0xCB51: BIT       2,        C\n", {0xCB, 0x51}},
    {"code0xCB52: BIT       2,        D\n", {0xCB, 0x52}},
    {"code0xCB53: BIT       2,        E\n", {0xCB, 0x53}},
    {"code0xCB54: BIT       2,        H\n", {0xCB, 0x54}},
    {"code0xCB55: BIT       2,        L\n", {0xCB, 0x55}},
    {"code0xCB56: BIT       2,     [HL]\n", {0xCB, 0x56}},
    {"code0xCB57: BIT       2,        A\n", {0xCB, 0x57}},
    {"code0xCB58: BIT       3,        B\n", {0xCB, 0x58}},
    {"code0xCB59: BIT       3,        C\n", {0xCB, 0x59}},
    {"code0xCB5A: BIT       3,        D\n", {0xCB, 0x5A}},
    {"code0xCB5B: BIT       3,        E\n", {0xCB, 0x5B}},
    {"code0xCB5C: BIT       3,        H\n", {0xCB, 0x5C}},
    {"code0xCB5D: BIT       3,        L\n", {0xCB, 0x5D}},
    {"code0xCB5E: BIT       3,     [HL]\n", {0xCB, 0x5E}},
    {"code0xCB5F: BIT       3,        A\n", {0xCB, 0x5F}},
    {"code0xCB60: BIT       4,        B\n", {0xCB, 0x60}},
    {"code0xCB61: BIT       4,        C\n", {0xCB, 0x61}},
    {"code0xCB62: BIT       4,        D\n", {0xCB, 0x62}},
    {"code0xCB63: BIT       4,        E\n", {0xCB, 0x63}},
    {"code0xCB64: BIT       4,        H\n", {0xCB, 0x64}},
    {"code0xCB65: BIT       4,        L\n", {0xCB, 0x65}},
    {"code0xCB66: BIT       4,     [HL]\n", {0xCB, 0x66}},
    {"code0xCB67: BIT       4,        A\n", {0xCB, 0x67}},
    {"code0xCB68: BIT       5,        B\n", {0xCB, 0x68}},
    {"code0xCB69: BIT       5,        C\n", {0xCB, 0x69}},
    {"code0xCB6A: BIT       5,        D\n", {0xCB, 0x6A}},
    {"code0xCB6B: BIT       5,        E\n", {0xCB, 0x6B}},
    {"code0xCB6C: BIT       5,        H\n", {0xCB, 0x6C}},
    {"code0xCB6D: BIT       5,        L\n", {0xCB, 0x6D}},
    {"code0xCB6E: BIT       5,     [HL]\n", {0xCB, 0x6E}},
    {"code0xCB6F: BIT       5,        A\n", {0xCB, 0x6F}},
    {"code0xCB70: BIT       6,        B\n", {0xCB, 0x70}},
    {"code0xCB71: BIT       6,        C\n", {0xCB, 0x71}},
    {"code0xCB72: BIT       6,        D\n", {0xCB, 0x72}},
    {"code0xCB73: BIT       6,        E\n", {0xCB, 0x73}},
    {"code0xCB74: BIT       6,        H\n", {0xCB, 0x74}},
    {"code0xCB75: BIT       6,        L\n", {0xCB, 0x75}},
    {"code0xCB76: BIT       6,     [HL]\n", {0xCB, 0x76}},
    {"code0xCB77: BIT       6,        A\n", {0xCB, 0x77}},
    {"code0xCB78: BIT       7,        B\n", {0xCB, 0x78}},
    {"code0xCB79: BIT       7,        C\n", {0xCB, 0x79}},
    {"code0xCB7A: BIT       7,        D\n", {0xCB, 0x7A}},
    {"code0xCB7B: BIT       7,        E\n", {0xCB, 0x7B}},
    {"code0xCB7C: BIT       7,        H\n", {0xCB, 0x7C}},
    {"code0xCB7D: BIT       7,        L\n", {0xCB, 0x7D}},
    {"code0xCB7E: BIT       7,     [HL]\n", {0xCB, 0x7E}},
    {"code0xCB7F: BIT       7,        A\n", {0xCB, 0x7F}},
    {"code0xCB80: RES       0,        B\n", {0xCB, 0x80}},
    {"code0xCB81: RES       0,        C\n", {0xCB, 0x81}},
    {"code0xCB82: RES       0,        D\n", {0xCB, 0x82}},
    {"code0xCB83: RES       0,        E\n", {0xCB, 0x83}},
    {"code0xCB84: RES       0,        H\n", {0xCB, 0x84}},
    {"code0xCB85: RES       0,        L\n", {0xCB, 0x85}},
    {"code0xCB86: RES       0,     [HL]\n", {0xCB, 0x86}},
    {"code0xCB87: RES       0,        A\n", {0xCB, 0x87}},
    {"code0xCB88: RES       1,        B\n", {0xCB, 0x88}},
    {"code0xCB89: RES       1,        C\n", {0xCB, 0x89}},
    {"code0xCB8A: RES       1,        D\n", {0xCB, 0x8A}},
    {"code0xCB8B: RES       1,        E\n", {0xCB, 0x8B}},
    {"code0xCB8C: RES       1,        H\n", {0xCB, 0x8C}},
    {"code0xCB8D: RES       1,        L\n", {0xCB, 0x8D}},
    {"code0xCB8E: RES       1,     [HL]\n", {0xCB, 0x8E}},
    {"code0xCB8F: RES       1,        A\n", {0xCB, 0x8F}},
    {"code0xCB90: RES       2,        B\n", {0xCB, 0x90}},
    {"code0xCB91: RES       2,        C\n", {0xCB, 0x91}},
    {"code0xCB92: RES       2,        D\n", {0xCB, 0x92}},
    {"code0xCB93: RES       2,        E\n", {0xCB, 0x93}},
    {"code0xCB94: RES       2,        H\n", {0xCB, 0x94}},
    {"code0xCB95: RES       2,        L\n", {0xCB, 0x95}},
    {"code0xCB96: RES       2,     [HL]\n", {0xCB, 0x96}},
    {"code0xCB97: RES       2,        A\n", {0xCB, 0x97}},
    {"code0xCB98: RES       3,        B\n", {0xCB, 0x98}},
    {"code0xCB99: RES       3,        C\n", {0xCB, 0x99}},
    {"code0xCB9A: RES       3,        D\n", {0xCB, 0x9A}},
    {"code0xCB9B: RES       3,        E\n", {0xCB, 0x9B}},
    {"code0xCB9C: RES       3,        H\n", {0xCB, 0x9C}},
    {"code0xCB9D: RES       3,        L\n", {0xCB, 0x9D}},
    {"code0xCB9E: RES       3,     [HL]\n", {0xCB, 0x9E}},
    {"code0xCB9F: RES       3,        A\n", {0xCB, 0x9F}},
    {"code0xCBA0: RES       4,        B\n", {0xCB, 0xA0}},
    {"code0xCBA1: RES       4,        C\n", {0xCB, 0xA1}},
    {"code0xCBA2: RES       4,        D\n", {0xCB, 0xA2}},
    {"code0xCBA3: RES       4,        E\n", {0xCB, 0xA3}},
    {"code0xCBA4: RES       4,        H\n", {0xCB, 0xA4}},
    {"code0xCBA5: RES       4,        L\n", {0xCB, 0xA5}},
    {"code0xCBA6: RES       4,     [HL]\n", {0xCB, 0xA6}},
    {"code0xCBA7: RES       4,        A\n", {0xCB, 0xA7}},
    {"code0xCBA8: RES       5,        B\n", {0xCB, 0xA8}},
    {"code0xCBA9: RES       5,        C\n", {0xCB, 0xA9}},
    {"code0xCBAA: RES       5,        D\n", {0xCB, 0xAA}},
    {"code0xCBAB: RES       5,        E\n", {0xCB, 0xAB}},
    {"code0xCBAC: RES       5,        H\n", {0xCB, 0xAC}},
    {"code0xCBAD: RES       5,        L\n", {0xCB, 0xAD}},
    {"code0xCBAE: RES       5,     [HL]\n", {0xCB, 0xAE}},
    {"code0xCBAF: RES       5,        A\n", {0xCB, 0xAF}},
    {"code0xCBB0: RES       6,        B\n", {0xCB, 0xB0}},
    {"code0xCBB1: RES       6,        C\n", {0xCB, 0xB1}},
    {"code0xCBB2: RES       6,        D\n", {0xCB, 0xB2}},
    {"code0xCBB3: RES       6,        E\n", {0xCB, 0xB3}},
    {"code0xCBB4: RES       6,        H\n", {0xCB, 0xB4}},
    {"code0xCBB5: RES       6,        L\n", {0xCB, 0xB5}},
    {"code0xCBB6: RES       6,     [HL]\n", {0xCB, 0xB6}},
    {"code0xCBB7: RES       6,        A\n", {0xCB, 0xB7}},
    {"code0xCBB8: RES       7,        B\n", {0xCB, 0xB8}},
    {"code0xCBB9: RES       7,        C\n", {0xCB, 0xB9}},
    {"code0xCBBA: RES       7,        D\n", {0xCB, 0xBA}},
    {"code0xCBBB: RES       7,        E\n", {0xCB, 0xBB}},
    {"code0xCBBC: RES       7,        H\n", {0xCB, 0xBC}},
    {"code0xCBBD: RES       7,        L\n", {0xCB, 0xBD}},
    {"code0xCBBE: RES       7,     [HL]\n", {0xCB, 0xBE}},
    {"code0xCBBF: RES       7,        A\n", {0xCB, 0xBF}},
    {"code0xCBC0: SET       0,        B\n", {0xCB, 0xC0}},
    {"code0xCBC1: SET       0,        C\n", {0xCB, 0xC1}},
    {"code0xCBC2: SET       0,        D\n", {0xCB, 0xC2}},
    {"code0xCBC3: SET       0,        E\n", {0xCB, 0xC3}},
    {"code0xCBC4: SET       0,        H\n", {0xCB, 0xC4}},
    {"code0xCBC5: SET       0,        L\n", {0xCB, 0xC5}},
    {"code0xCBC6: SET       0,     [HL]\n", {0xCB, 0xC6}},
    {"code0xCBC7: SET       0,        A\n", {0xCB, 0xC7}},
    {"code0xCBC8: SET       1,        B\n", {0xCB, 0xC8}},
    {"code0xCBC9: SET       1,        C\n", {0xCB, 0xC9}},
    {"code0xCBCA: SET       1,        D\n", {0xCB, 0xCA}},
    {"code0xCBCB: SET       1,        E\n", {0xCB, 0xCB}},
    {"code0xCBCC: SET       1,        H\n", {0xCB, 0xCC}},
    {"code0xCBCD: SET       1,        L\n", {0xCB, 0xCD}},
    {"code0xCBCE: SET       1,     [HL]\n", {0xCB, 0xCE}},
    {"code0xCBCF: SET       1,        A\n", {0xCB, 0xCF}},
    {"code0xCBD0: SET       2,        B\n", {0xCB, 0xD0}},
    {"code0xCBD1: SET       2,        C\n", {0xCB, 0xD1}},
    {"code0xCBD2: SET       2,        D\n", {0xCB, 0xD2}},
    {"code0xCBD3: SET       2,        E\n", {0xCB, 0xD3}},
    {"code0xCBD4: SET       2,        H\n", {0xCB, 0xD4}},
    {"code0xCBD5: SET       2,        L\n", {0xCB, 0xD5}},
    {"code0xCBD6: SET       2,     [HL]\n", {0xCB, 0xD6}},
    {"code0xCBD7: SET       2,        A\n", {0xCB, 0xD7}},
    {"code0xCBD8: SET       3,        B\n", {0xCB, 0xD8}},
    {"code0xCBD9: SET       3,        C\n", {0xCB, 0xD9}},
    {"code0xCBDA: SET       3,        D\n", {0xCB, 0xDA}},
    {"code0xCBDB: SET       3,        E\n", {0xCB, 0xDB}},
    {"code0xCBDC: SET       3,        H\n", {0xCB, 0xDC}},
    {"code0xCBDD: SET       3,        L\n", {0xCB, 0xDD}},
    {"code0xCBDE: SET       3,     [HL]\n", {0xCB, 0xDE}},
    {"code0xCBDF: SET       3,        A\n", {0xCB, 0xDF}},
    {"code0xCBE0: SET       4,        B\n", {0xCB, 0xE0}},
    {"code0xCBE1: SET       4,        C\n", {0xCB, 0xE1}},
    {"code0xCBE2: SET       4,        D\n", {0xCB, 0xE2}},
    {"code0xCBE3: SET       4,        E\n", {0xCB, 0xE3}},
    {"code0xCBE4: SET       4,        H\n", {0xCB, 0xE4}},
    {"code0xCBE5: SET       4,        L\n", {0xCB, 0xE5}},
    {"code0xCBE6: SET       4,     [HL]\n", {0xCB, 0xE6}},
    {"code0xCBE7: SET       4,        A\n", {0xCB, 0xE7}},
    {"code0xCBE8: SET       5,        B\n", {0xCB, 0xE8}},
    {"code0xCBE9: SET       5,        C\n", {0xCB, 0xE9}},
    {"code0xCBEA: SET       5,        D\n", {0xCB, 0xEA}},
    {"code0xCBEB: SET       5,        E\n", {0xCB, 0xEB}},
    {"code0xCBEC: SET       5,        H\n", {0xCB, 0xEC}},
    {"code0xCBED: SET       5,        L\n", {0xCB, 0xED}},
    {"code0xCBEE: SET       5,     [HL]\n", {0xCB, 0xEE}},
    {"code0xCBEF: SET       5,        A\n", {0xCB, 0xEF}},
    {"code0xCBF0: SET       6,        B\n", {0xCB, 0xF0}},
    {"code0xCBF1: SET       6,        C\n", {0xCB, 0xF1}},
    {"code0xCBF2: SET       6,        D\n", {0xCB, 0xF2}},
    {"code0xCBF3: SET       6,        E\n", {0xCB, 0xF3}},
    {"code0xCBF4: SET       6,        H\n", {0xCB, 0xF4}},
    {"code0xCBF5: SET       6,        L\n", {0xCB, 0xF5}},
    {"code0xCBF6: SET       6,     [HL]\n", {0xCB, 0xF6}},
    {"code0xCBF7: SET       6,        A\n", {0xCB, 0xF7}},
    {"code0xCBF8: SET       7,        B\n", {0xCB, 0xF8}},
    {"code0xCBF9: SET       7,        C\n", {0xCB, 0xF9}},
    {"code0xCBFA: SET       7,        D\n", {0xCB, 0xFA}},
    {"code0xCBFB: SET       7,        E\n", {0xCB, 0xFB}},
    {"code0xCBFC: SET       7,        H\n", {0xCB, 0xFC}},
    {"code0xCBFD: SET       7,        L\n", {0xCB, 0xFD}},
    {"code0xCBFE: SET       7,     [HL]\n", {0xCB, 0xFE}},
    {"code0xCBFF: SET       7,        A\n", {0xCB, 0xFF}},
    {"code0xCC:   CALL      Z,   0xFFFF\n", {0xCC, 0xFF, 0xFF}},
    {"code0xCD:   CALL           0xFFFF\n", {0xCD, 0xFF, 0xFF}},
    {"code0xCE:   ADC       A,     0xFF\n", {0xCE, 0xFF}},
    {"code0xCF:   RST              0x08\n", {0xCF}},
    {"code0xD0:   RET      NC          \n", {0xD0}},
    {"code0xD1:   POP      DE          \n", {0xD1}},
    {"code0xD2:   JP       NC,   0xFFFF\n", {0xD2, 0xFF, 0xFF}},
    // code0xD3 not exist
    {"code0xD4:   CALL     NC,   0xFFFF\n", {0xD4, 0xFF, 0xFF}},
    {"code0xD5:   PUSH     DE          \n", {0xD5}},
    {"code0xD6:   SUB    0xFF          \n", {0xD6, 0xFF}},
    {"code0xD7:   RST              0x10\n", {0xD7}},
    {"code0xD8:   RET       C          \n", {0xD8}},
    {"code0xD9:   RETI                 \n", {0xD9}},
    {"code0xDA:   JP        C,   0xFFFF\n", {0xDA, 0xFF, 0xFF}},
    // code0xDB not exist
    {"code0xDC:   CALL      C,   0xFFFF\n", {0xDC, 0xFF, 0xFF}},
    // code0xDD not exist
    {"code0xDE:   SBC       A,     0xFF\n", {0xDE, 0xFF}},
    {"code0xDF:   RST              0x18\n", {0xDF}},
    {"code0xE0:   LDH  [0xFF],        A\n", {0xE0, 0xFF}},
    {"code0xE1:   POP      HL          \n", {0xE1}},
    {"code0xE2:   LD      [C],        A\n", {0xE2}},
    // code0xE3 not exist
    // code0xE4 not exist
    {"code0xE5:   PUSH     HL          \n", {0xE5}},
    {"code0xE6:   AND    0xFF          \n", {0xE6, 0xFF}},
    {"code0xE7:   RST              0x20\n", {0xE7}},
    {"code0xE8:   ADD      SP,     0xFF\n", {0xE8, 0xFF}},
    {"code0xE9:   JP                 HL\n", {0xE9}},
    {"code0xEA:   LD [0xFFFF],        A\n", {0xEA, 0xFF, 0xFF}},
    // code0xEB not exist
    // code0xEC not exist
    // code0xED not exist
    {"code0xEE:   XOR    0xFF          \n", {0xEE, 0xFF}},
    {"code0xEF:   RST              0x28\n", {0xEF}},
    {"code0xF0:   LDH       A,   [0xFF]\n", {0xF0, 0xFF}},
    {"code0xF1:   POP      AF          \n", {0xF1}},
    {"code0xF2:   LD        A,      [C]\n", {0xF2}},
    {"code0xF3:   DI                   \n", {0xF3}},
    // code0xF4 not exist
    {"code0xF5:   PUSH     AF          \n", {0xF5}},
    {"code0xF6:   OR     0xFF          \n", {0xF6, 0xFF}},
    {"code0xF7:   RST              0x30\n", {0xF7}},
    // code0xF8 unimplemented
    {"code0xF9:   LD       SP,       HL\n", {0xF9}},
    {"code0xFA:   LD        A, [0xFFFF]\n", {0xFA, 0xFF, 0xFF}},
    {"code0xFB:   EI                   \n", {0xFB}},
    // code0xFC not exist
    // code0xFD not exist
    {"code0xFE:   CP     0xFF          \n", {0xFE, 0xFF}},
    {"code0xFF:   RST              0x38\n", {0xFF}},
    {"codexxxx:   DB               0x80\n", {0x80}},
    {"codexxxx:   DB   0x00, 0x10, 0x20\n", {0x00, 0x10, 0x20}},
    {"codexxxx:   DS               0x01\n", {0x00}},
    {"codexxxx:   DS               0x04\n", {0x00, 0x00, 0x00, 0x00}},
    {"codexxxx:   DP               0x02\n", {0x00, 0x00}},
    {"codexxxx:   DP               0x04\n", {0x00, 0x00, 0x00, 0x00}}};

INSTANTIATE_TEST_SUITE_P(AssemblerTest, MnemonicTest,
                         ::testing::ValuesIn(kTestCodes));