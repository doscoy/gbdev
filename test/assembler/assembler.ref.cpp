#include "generator_impl.hpp"

#include <gtest/gtest.h>

#include <fstream>
#include <sstream>

#include "myutil.hpp"

class MnemRefTest
    : public ::testing::TestWithParam<std::pair<std::string, std::vector<unsigned char>>> {
 protected:
  void SetUp() override {
    std::string src{};
    std::tie(src, answer_) = GetParam();
    source_.append("addr0000:\n");
    source_.append("ds 0x10\n");
    source_.append("addr0010:\n");
    source_.append("ds 0xF0\n");
    source_.append("addr0100:\n");
    source_.append("def myvalue, 0xDEAD\n");
    source_.append(src);
  }
  std::string source_;
  std::vector<unsigned char> answer_;
};

TEST_P(MnemRefTest, TranslateCheck) {
  GBDev::AssemblerImpl::Tokenizer tokenizer{source_};
  GBDev::AssemblerImpl::Parser parser{tokenizer.GetToken()};
  GBDev::AssemblerImpl::Generator generator{parser.GetNodes()};
  auto& data{generator.GetData()};
  auto bin{data.os.rawbuf()};
  auto counter{0};
  if (bin.size() != answer_.size() + 0x100) {
    std::cout << "size error\n";
    FAIL();
  }
  for (auto c = bin.begin() + 0x100; c < bin.end(); ++c) {
    std::cout << std::hex << std::uppercase << std::setw(2) << std::setfill('0')
              << (int)(unsigned char)*c << " ";
    ASSERT_EQ(answer_[counter], (unsigned char)*c);
    ++counter;
  }
  std::cout << "\n";
}

const std::pair<std::string, std::vector<unsigned char>> kRefTestCodes[]{
    {"LD BC,   addr0000     \n", {0x01, 0x00, 0x00 }},
    {"LD B,    addr0000     \n", {0x06, 0x00}},
    {"LD      [addr0000], SP\n", {0x08, 0x00, 0x00 }},
    {"LD C,    addr0000     \n", {0x0E, 0x00}},
    {"LD DE,   addr0000     \n", {0x11, 0x00, 0x00 }},
    {"LD D,    addr0000     \n", {0x16, 0x00}},
    {"LD E,    addr0000     \n", {0x1E, 0x00}},
    {"LD HL,   addr0000     \n", {0x21, 0x00, 0x00 }},
    {"LD H,    addr0000     \n", {0x26, 0x00}},
    {"LD L,    addr0000     \n", {0x2E, 0x00}},
    {"LD SP,   addr0000     \n", {0x31, 0x00, 0x00 }},
    {"LD [HL], addr0000     \n", {0x36, 0x00}},
    {"LD A,    addr0000     \n", {0x3E, 0x00}},
    {"LDH     [addr0000],  A\n", {0xE0, 0x00}},
    {"LD      [addr0000],  A\n", {0xEA, 0x00, 0x00 }},
    {"LDH A,  [addr0000]    \n", {0xF0, 0x00}},
    {"LD A,   [addr0000]    \n", {0xFA, 0x00, 0x00 }},
    {"ADD A,   addr0000     \n", {0xC6, 0x00}},
    {"ADD SP,  addr0000     \n", {0xE8, 0x00}},
    {"ADC A,   addr0000     \n", {0xCE, 0x00}},
    {"SUB      addr0000     \n", {0xD6, 0x00}},
    {"SBC A,   addr0000     \n", {0xDE, 0x00}},
    {"JP NZ,   addr0000     \n", {0xC2, 0x00, 0x00 }},
    {"JP       addr0000     \n", {0xC3, 0x00, 0x00 }},
    {"JP Z,    addr0000     \n", {0xCA, 0x00, 0x00 }},
    {"JP NC,   addr0000     \n", {0xD2, 0x00, 0x00 }},
    {"JP C,    addr0000     \n", {0xDA, 0x00, 0x00 }},
    {"JR       addr0000     \n", {0x18, 0x00}},
    {"JR NZ,   addr0000     \n", {0x20, 0x00}},
    {"JR Z,    addr0000     \n", {0x28, 0x00}},
    {"JR NC,   addr0000     \n", {0x30, 0x00}},
    {"JR C,    addr0000     \n", {0x38, 0x00}},
    {"CALL NZ, addr0000     \n", {0xC4, 0x00, 0x00 }},
    {"CALL Z,  addr0000     \n", {0xCC, 0x00, 0x00 }},
    {"CALL     addr0000     \n", {0xCD, 0x00, 0x00 }},
    {"CALL NC, addr0000     \n", {0xD4, 0x00, 0x00 }},
    {"CALL C,  addr0000     \n", {0xDC, 0x00, 0x00 }},
    {"LD BC,   addr0010     \n", {0x01, 0x10, 0x00 }},
    {"LD B,    addr0010     \n", {0x06, 0x10}},
    {"LD      [addr0010], SP\n", {0x08, 0x10, 0x00 }},
    {"LD C,    addr0010     \n", {0x0E, 0x10}},
    {"LD DE,   addr0010     \n", {0x11, 0x10, 0x00 }},
    {"LD D,    addr0010     \n", {0x16, 0x10}},
    {"LD E,    addr0010     \n", {0x1E, 0x10}},
    {"LD HL,   addr0010     \n", {0x21, 0x10, 0x00 }},
    {"LD H,    addr0010     \n", {0x26, 0x10}},
    {"LD L,    addr0010     \n", {0x2E, 0x10}},
    {"LD SP,   addr0010     \n", {0x31, 0x10, 0x00 }},
    {"LD [HL], addr0010     \n", {0x36, 0x10}},
    {"LD A,    addr0010     \n", {0x3E, 0x10}},
    {"LDH     [addr0010],  A\n", {0xE0, 0x10}},
    {"LD      [addr0010],  A\n", {0xEA, 0x10, 0x00 }},
    {"LDH A,  [addr0010]    \n", {0xF0, 0x10}},
    {"LD A,   [addr0010]    \n", {0xFA, 0x10, 0x00 }},
    {"ADD A,   addr0010     \n", {0xC6, 0x10}},
    {"ADD SP,  addr0010     \n", {0xE8, 0x10}},
    {"ADC A,   addr0010     \n", {0xCE, 0x10}},
    {"SUB      addr0010     \n", {0xD6, 0x10}},
    {"SBC A,   addr0010     \n", {0xDE, 0x10}},
    {"JP NZ,   addr0010     \n", {0xC2, 0x10, 0x00 }},
    {"JP       addr0010     \n", {0xC3, 0x10, 0x00 }},
    {"JP Z,    addr0010     \n", {0xCA, 0x10, 0x00 }},
    {"JP NC,   addr0010     \n", {0xD2, 0x10, 0x00 }},
    {"JP C,    addr0010     \n", {0xDA, 0x10, 0x00 }},
    {"JR       addr0010     \n", {0x18, 0x10}},
    {"JR NZ,   addr0010     \n", {0x20, 0x10}},
    {"JR Z,    addr0010     \n", {0x28, 0x10}},
    {"JR NC,   addr0010     \n", {0x30, 0x10}},
    {"JR C,    addr0010     \n", {0x38, 0x10}},
    {"CALL NZ, addr0010     \n", {0xC4, 0x10, 0x00 }},
    {"CALL Z,  addr0010     \n", {0xCC, 0x10, 0x00 }},
    {"CALL     addr0010     \n", {0xCD, 0x10, 0x00 }},
    {"CALL NC, addr0010     \n", {0xD4, 0x10, 0x00 }},
    {"CALL C,  addr0010     \n", {0xDC, 0x10, 0x00 }},
    {"LD BC,   addr0100     \n", {0x01, 0x00, 0x01 }},
    {"LD B,    addr0100     \n", {0x06, 0x00}},
    {"LD      [addr0100], SP\n", {0x08, 0x00, 0x01 }},
    {"LD C,    addr0100     \n", {0x0E, 0x00}},
    {"LD DE,   addr0100     \n", {0x11, 0x00, 0x01 }},
    {"LD D,    addr0100     \n", {0x16, 0x00}},
    {"LD E,    addr0100     \n", {0x1E, 0x00}},
    {"LD HL,   addr0100     \n", {0x21, 0x00, 0x01 }},
    {"LD H,    addr0100     \n", {0x26, 0x00}},
    {"LD L,    addr0100     \n", {0x2E, 0x00}},
    {"LD SP,   addr0100     \n", {0x31, 0x00, 0x01 }},
    {"LD [HL], addr0100     \n", {0x36, 0x00}},
    {"LD A,    addr0100     \n", {0x3E, 0x00}},
    {"LDH     [addr0100],  A\n", {0xE0, 0x00}},
    {"LD      [addr0100],  A\n", {0xEA, 0x00, 0x01 }},
    {"LDH A,  [addr0100]    \n", {0xF0, 0x00}},
    {"LD A,   [addr0100]    \n", {0xFA, 0x00, 0x01 }},
    {"ADD A,   addr0100     \n", {0xC6, 0x00}},
    {"ADD SP,  addr0100     \n", {0xE8, 0x00}},
    {"ADC A,   addr0100     \n", {0xCE, 0x00}},
    {"SUB      addr0100     \n", {0xD6, 0x00}},
    {"SBC A,   addr0100     \n", {0xDE, 0x00}},
    {"JP NZ,   addr0100     \n", {0xC2, 0x00, 0x01 }},
    {"JP       addr0100     \n", {0xC3, 0x00, 0x01 }},
    {"JP Z,    addr0100     \n", {0xCA, 0x00, 0x01 }},
    {"JP NC,   addr0100     \n", {0xD2, 0x00, 0x01 }},
    {"JP C,    addr0100     \n", {0xDA, 0x00, 0x01 }},
    {"JR       addr0100     \n", {0x18, 0x00}},
    {"JR NZ,   addr0100     \n", {0x20, 0x00}},
    {"JR Z,    addr0100     \n", {0x28, 0x00}},
    {"JR NC,   addr0100     \n", {0x30, 0x00}},
    {"JR C,    addr0100     \n", {0x38, 0x00}},
    {"CALL NZ, addr0100     \n", {0xC4, 0x00, 0x01 }},
    {"CALL Z,  addr0100     \n", {0xCC, 0x00, 0x01 }},
    {"CALL     addr0100     \n", {0xCD, 0x00, 0x01 }},
    {"CALL NC, addr0100     \n", {0xD4, 0x00, 0x01 }},
    {"CALL C,  addr0100     \n", {0xDC, 0x00, 0x01 }},
    {"LD BC,   myvalue      \n", {0x01, 0xAD, 0xDE }},
    {"LD B,    myvalue      \n", {0x06, 0xAD}},
    {"LD      [myvalue],  SP\n", {0x08, 0xAD, 0xDE }},
    {"LD C,    myvalue      \n", {0x0E, 0xAD}},
    {"LD DE,   myvalue      \n", {0x11, 0xAD, 0xDE }},
    {"LD D,    myvalue      \n", {0x16, 0xAD}},
    {"LD E,    myvalue      \n", {0x1E, 0xAD}},
    {"LD HL,   myvalue      \n", {0x21, 0xAD, 0xDE }},
    {"LD H,    myvalue      \n", {0x26, 0xAD}},
    {"LD L,    myvalue      \n", {0x2E, 0xAD}},
    {"LD SP,   myvalue      \n", {0x31, 0xAD, 0xDE }},
    {"LD [HL], myvalue      \n", {0x36, 0xAD}},
    {"LD A,    myvalue      \n", {0x3E, 0xAD}},
    {"LDH     [myvalue],   A\n", {0xE0, 0xAD}},
    {"LD      [myvalue],   A\n", {0xEA, 0xAD, 0xDE }},
    {"LDH A,  [myvalue]     \n", {0xF0, 0xAD}},
    {"LD A,   [myvalue]     \n", {0xFA, 0xAD, 0xDE }},
    {"ADD A,   myvalue      \n", {0xC6, 0xAD}},
    {"ADD SP,  myvalue      \n", {0xE8, 0xAD}},
    {"ADC A,   myvalue      \n", {0xCE, 0xAD}},
    {"SUB      myvalue      \n", {0xD6, 0xAD}},
    {"SBC A,   myvalue      \n", {0xDE, 0xAD}},
    {"JP NZ,   myvalue      \n", {0xC2, 0xAD, 0xDE }},
    {"JP       myvalue      \n", {0xC3, 0xAD, 0xDE }},
    {"JP Z,    myvalue      \n", {0xCA, 0xAD, 0xDE }},
    {"JP NC,   myvalue      \n", {0xD2, 0xAD, 0xDE }},
    {"JP C,    myvalue      \n", {0xDA, 0xAD, 0xDE }},
    {"JR       myvalue      \n", {0x18, 0xAD}},
    {"JR NZ,   myvalue      \n", {0x20, 0xAD}},
    {"JR Z,    myvalue      \n", {0x28, 0xAD}},
    {"JR NC,   myvalue      \n", {0x30, 0xAD}},
    {"JR C,    myvalue      \n", {0x38, 0xAD}},
    {"CALL NZ, myvalue      \n", {0xC4, 0xAD, 0xDE }},
    {"CALL Z,  myvalue      \n", {0xCC, 0xAD, 0xDE }},
    {"CALL     myvalue      \n", {0xCD, 0xAD, 0xDE }},
    {"CALL NC, myvalue      \n", {0xD4, 0xAD, 0xDE }},
    {"CALL C,  myvalue      \n", {0xDC, 0xAD, 0xDE }},
};

INSTANTIATE_TEST_SUITE_P(AssemblerTest, MnemRefTest,
                         ::testing::ValuesIn(kRefTestCodes));