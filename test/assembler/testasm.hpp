#pragma once

constexpr char kTestAsm[]{
    ";(hl = (word*)write_addr, bc = (word*)read_addr, de = (word)length)\n"
    "MemCopy:\n"
    "ld a, [bc]\n"
    "ldi [hl], a\n"
    "inc bc\n"
    "dec de\n"
    "ld a, d\n"
    "or a, e\n"
    "jp nz, MemCopy\n"
    "ret\n"};

constexpr char kTestFormla[]{
    "def v1, 1\n"
    "def v2, 2\n"
    "def v3, (v1 + v2) * 3\n"
    "def len, DataEnd - DataBeg\n"
    "DataBeg:\n"
    "db 0x48, 0x65, 0x6C, 0x6C, 0x6F\n"
    "DataEnd:\n"
    "db v1, v2, v3, len\n"};

constexpr char kTestError[]{
    "ld a, 3.14\n"
    "ld [bc, [a\n"
    "ld a, 80h\n"
    "ld [80h], \n"
    "db,,,,,,,\n"
    "def x, x\n"
    "def y, 1 + y\n"
    "ld a, not_def\n"
    "ld a, 1 + not_def\n"
    "ld a, not_def + 1\n"
    "ld a, 1 + not_def + 1\n"
    "dp 1\n"};