#include <gtest/gtest.h>

#include <fstream>
#include <sstream>

#include "generator_impl.hpp"
#include "myutil.hpp"

void HexOut(const std::vector<char>& buf, std::ostream* os) {
  auto* row_head{buf.data()};
  for (const auto& item : buf) {
    *os << std::hex << std::uppercase << std::setw(2) << std::setfill('0')
        << (int)(unsigned char)item;
    if (&item - row_head < 15) {
      *os << " ";
    } else {
      *os << "\n";
      row_head = &item + 1;
    }
  }
}

class MnemErrTest : public ::testing::TestWithParam<
                         std::string_view> {
 protected:
  void SetUp() override {
    source_ = GetParam();
    GBDev::AssemblerImpl::Tokenizer tokenizer{source_};
    GBDev::AssemblerImpl::Parser parser{tokenizer.GetToken()};
    GBDev::AssemblerImpl::Generator generator{parser.GetNodes()};
    auto& data{generator.GetData()};
    std::stringstream oss;
    if (generator.GetErrorCount() == 0) {
      err_ = false;
      oss << "ok: " << source_ << "\n";
      HexOut(data.os.rawbuf(), &oss);
      assert_ = data.os.rawbuf().size() == 0;
    } else {
      err_ = true;
      const auto err{generator.GetErrors()};
      GBDev::ErrorMsgHelper::Output(*err.begin(), source_, &oss);
      if (source_.substr(0, 2) != "DB") {
        assert_ = data.os.rawbuf().size() > 0;
      } else {
        assert_ = false;
      }
    }
    output_ = oss.str();
    }
  std::string source_;
  std::string output_;
  bool assert_;
  bool err_;
};

namespace {
std::fstream mylog_pass("../../../test/assembler/.pass.log", std::ios_base::out);
std::fstream mylog_err("../../../test/assembler/.err.log", std::ios_base::out);
}  // namespace

TEST_P(MnemErrTest, TranslateCheck) {
  std::cout << source_ << "\n";
  ASSERT_FALSE(assert_);
  if (err_) {
    mylog_err << output_;
  } else {
    mylog_pass << output_;
  }
}

constexpr std::string_view kTestCodes[]{
#include "errortestcode"
};

INSTANTIATE_TEST_SUITE_P(AssemblerTest, MnemErrTest,
                         ::testing::ValuesIn(kTestCodes));