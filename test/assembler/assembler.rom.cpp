#include "assembler.hpp"

#include <gtest/gtest.h>

#include <array>
#include <filesystem>
#include <fstream>

TEST(AssemblerTest, RomTest) {
  GBDev::Assembler assembler;
  std::filesystem::path src_path{
      "../../../src/wasm/resources/init.gbasm"};
  const int id{assembler.Input(src_path)};
  if (id < 0) {
    FAIL() << "cannot open " << src_path << "\n";
  }
  using sib = std::ios_base;
  const std::filesystem::path kOutputDirPath{"../../../test/assembler"};
  std::array<std::filesystem::path, 3> log_paths{
      kOutputDirPath / ".testrom.token", kOutputDirPath / ".testrom.node",
      kOutputDirPath / ".testrom.data"};
  std::array<std::unique_ptr<std::fstream>, log_paths.size()> log_outputs;
  int count{0};
  for (const auto& log_path : log_paths) {
    log_outputs[count] =
        std::make_unique<std::fstream>(log_path, sib::out | sib::binary);
    if (!log_outputs[count]->is_open()) {
      FAIL() << "cannot open " << log_path << "\n";
    }
    ++count;
  };
  int errcnt{assembler.Assemble(log_outputs[0].get(), log_outputs[1].get(),
                                log_outputs[2].get())};
  if (errcnt != 0) {
    std::fstream input(src_path, sib::in | sib::binary);
    if (input.is_open()) {
      std::stringstream ss;
      ss << input.rdbuf();
      auto errs{assembler.GetErrors()};
      for (const auto& err : errs) {
        GBDev::ErrorMsgHelper::Output(err, ss.str(), &std::cout);
      }
    }
    FAIL();
  }
  std::filesystem::path output_path{kOutputDirPath / ".testrom.gb"};
  std::fstream output(output_path, sib::out | sib::binary);
  if (!output.is_open()) {
    FAIL() << "cannot open " << output_path << "\n";
  }
  auto bin{assembler.GetRom()};
  for (const auto& c : bin) {
    output.write(&c, 1);
  }
}