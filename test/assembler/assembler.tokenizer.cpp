#include <gtest/gtest.h>

#include <iostream>

#include "testasm.hpp"
#include "tokenizer_impl.hpp"

TEST(AssemblerTest, AsmTokenizerCheck) {
  std::string src{kTestAsm};
  GBDev::AssemblerImpl::Tokenizer tokenizer{src};
  using Token = typename GBDev::AssemblerImpl::Tokenizer::Token;
  constexpr std::pair<Token::Kind, std::string_view> kAns[]{
      {Token::Kind::STRING, "MemCopy"},
      {Token::Kind::SYMBOL, ":"},   {Token::Kind::EOL, "\n"},
      {Token::Kind::STRING, "ld"},  {Token::Kind::STRING, "a"},
      {Token::Kind::SYMBOL, ","},   {Token::Kind::SYMBOL, "["},
      {Token::Kind::STRING, "bc"},  {Token::Kind::SYMBOL, "]"},
      {Token::Kind::EOL, "\n"},     {Token::Kind::STRING, "ldi"},
      {Token::Kind::SYMBOL, "["},   {Token::Kind::STRING, "hl"},
      {Token::Kind::SYMBOL, "]"},   {Token::Kind::SYMBOL, ","},
      {Token::Kind::STRING, "a"},   {Token::Kind::EOL, "\n"},
      {Token::Kind::STRING, "inc"}, {Token::Kind::STRING, "bc"},
      {Token::Kind::EOL, "\n"},     {Token::Kind::STRING, "dec"},
      {Token::Kind::STRING, "de"},  {Token::Kind::EOL, "\n"},
      {Token::Kind::STRING, "ld"},  {Token::Kind::STRING, "a"},
      {Token::Kind::SYMBOL, ","},   {Token::Kind::STRING, "d"},
      {Token::Kind::EOL, "\n"},     {Token::Kind::STRING, "or"},
      {Token::Kind::STRING, "a"},   {Token::Kind::SYMBOL, ","},
      {Token::Kind::STRING, "e"},   {Token::Kind::EOL, "\n"},
      {Token::Kind::STRING, "jp"},  {Token::Kind::STRING, "nz"},
      {Token::Kind::SYMBOL, ","},   {Token::Kind::STRING, "MemCopy"},
      {Token::Kind::EOL, "\n"},     {Token::Kind::STRING, "ret"},
      {Token::Kind::EOL, "\n"},     {Token::Kind::EOL, ""},
  };
  auto token{tokenizer.GetToken()};
  auto itr{token.begin()};
  auto end{token.end()};
  for (int i = 0; itr < end; ++i, ++itr) {
    if (i >= static_cast<int>(sizeof(kAns) / sizeof(kAns[0]))) {
      std::cout << "wrong kAns\n";
      FAIL();
    }
    auto str{itr->str != "\n" ? itr->str : "\\n"};
    std::cout << i << ", " << str << "\n";
    ASSERT_EQ(kAns[i].first, itr->kind);
    ASSERT_EQ(kAns[i].second, itr->str);
  }
}

TEST(AssemblerTest, FormulaTokenizerCheck) {
  std::string src{kTestFormla};
  GBDev::AssemblerImpl::Tokenizer tokenizer{src};
  using Token = typename GBDev::AssemblerImpl::Tokenizer::Token;
  constexpr std::pair<Token::Kind, std::string_view> kAns[]{
      {Token::Kind::STRING, "def"},     {Token::Kind::STRING, "v1"},
      {Token::Kind::SYMBOL, ","},       {Token::Kind::NUMBER, "1"},
      {Token::Kind::EOL, "\n"},         {Token::Kind::STRING, "def"},
      {Token::Kind::STRING, "v2"},      {Token::Kind::SYMBOL, ","},
      {Token::Kind::NUMBER, "2"},       {Token::Kind::EOL, "\n"},
      {Token::Kind::STRING, "def"},     {Token::Kind::STRING, "v3"},
      {Token::Kind::SYMBOL, ","},       {Token::Kind::SYMBOL, "("},
      {Token::Kind::STRING, "v1"},      {Token::Kind::SYMBOL, "+"},
      {Token::Kind::STRING, "v2"},      {Token::Kind::SYMBOL, ")"},
      {Token::Kind::SYMBOL, "*"},       {Token::Kind::NUMBER, "3"},
      {Token::Kind::EOL, "\n"},         {Token::Kind::STRING, "def"},
      {Token::Kind::STRING, "len"},     {Token::Kind::SYMBOL, ","},
      {Token::Kind::STRING, "DataEnd"}, {Token::Kind::SYMBOL, "-"},
      {Token::Kind::STRING, "DataBeg"}, {Token::Kind::EOL, "\n"},
      {Token::Kind::STRING, "DataBeg"}, {Token::Kind::SYMBOL, ":"},
      {Token::Kind::EOL, "\n"},         {Token::Kind::STRING, "db"},
      {Token::Kind::PREHEX, "0x"},      {Token::Kind::NUMBER, "48"},
      {Token::Kind::SYMBOL, ","},       {Token::Kind::PREHEX, "0x"},
      {Token::Kind::NUMBER, "65"},      {Token::Kind::SYMBOL, ","},
      {Token::Kind::PREHEX, "0x"},      {Token::Kind::NUMBER, "6C"},
      {Token::Kind::SYMBOL, ","},       {Token::Kind::PREHEX, "0x"},
      {Token::Kind::NUMBER, "6C"},      {Token::Kind::SYMBOL, ","},
      {Token::Kind::PREHEX, "0x"},      {Token::Kind::NUMBER, "6F"},
      {Token::Kind::EOL, "\n"},         {Token::Kind::STRING, "DataEnd"},
      {Token::Kind::SYMBOL, ":"},       {Token::Kind::EOL, "\n"},
      {Token::Kind::STRING, "db"},      {Token::Kind::STRING, "v1"},
      {Token::Kind::SYMBOL, ","},       {Token::Kind::STRING, "v2"},
      {Token::Kind::SYMBOL, ","},       {Token::Kind::STRING, "v3"},
      {Token::Kind::SYMBOL, ","},       {Token::Kind::STRING, "len"},
      {Token::Kind::EOL, "\n"},         {Token::Kind::EOL, ""}};
  auto token{tokenizer.GetToken()};
  auto itr{token.begin()};
  auto end{token.end()};
  for (int i = 0; itr < end; ++i, ++itr) {
    if (i >= static_cast<int>(sizeof(kAns) / sizeof(kAns[0]))) {
      std::cout << "wrong kAns\n";
      FAIL();
    }
    auto str{itr->str != "\n" ? itr->str : "\\n"};
    std::cout << i << ", " << str << "\n";
    ASSERT_EQ(kAns[i].first, itr->kind);
    ASSERT_EQ(kAns[i].second, itr->str);
  }
}

TEST(AssemblerTest, TokenizerErrorCheck) {
  std::vector<std::string> srcs{kTestAsm, kTestFormla, kTestError};
  GBDev::AssemblerImpl::Tokenizer tokenizer;
  for (auto& src: srcs) {
    tokenizer.SetSource(src);
  }
  ASSERT_NE(0, tokenizer.GetErrorCount());
  auto errs{tokenizer.GetErrors()};
  for (const auto& err : errs) {
    GBDev::ErrorMsgHelper::Output(err, srcs[err.cursor.src], &std::cout);
  }
}