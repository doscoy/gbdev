#include <gtest/gtest.h>

#include <iostream>

#include "generator_impl.hpp"
#include "testasm.hpp"

TEST(AssemblerTest, AsmGeneratorCheck) {
  std::string src{kTestAsm};
  GBDev::AssemblerImpl::Tokenizer tokenizer{src};
  GBDev::AssemblerImpl::Parser parser{tokenizer.GetToken()};
  GBDev::AssemblerImpl::Generator generator{parser.GetNodes()};
  constexpr unsigned char kAns[]{
      // MemCopy: (0x0000)
      0x0A,              // ld a, [bc]
      0x22,              // ldi [hl], a
      0x03,              // inc bc
      0x1B,              // dec de
      0x7A,              // ld a, d
      0xB3,              // or a, e
      0xC2, 0x00, 0x00,  // jp nz, MemCopy
      0xC9               // ret
  };
  auto& data{generator.GetData()};
  auto bin{data.os.rawbuf()};
  auto counter{0};
  for (const auto& c : bin) {
    std::cout << std::hex << std::uppercase << std::setw(2) << std::setfill('0')
              << (int)(unsigned char)c << " ";
    ASSERT_EQ(kAns[counter], (unsigned char)c);
    ++counter;
  }
  std::cout << std::dec << "\n";
}

TEST(AssemblerTest, FormulaGeneratorCheck) {
  std::string src{kTestFormla};
  GBDev::AssemblerImpl::Tokenizer tokenizer{src};
  GBDev::AssemblerImpl::Parser parser{tokenizer.GetToken()};
  GBDev::AssemblerImpl::Generator generator{parser.GetNodes()};
  constexpr unsigned char kAns[]{
      // DataBeg: (0x0000)
      0x48, 0x65, 0x6C, 0x6C, 0x6F,
      // DataEnd: (0x0005)
      0x01, // v1 = 1
      0x02, // v2 = 2
      0x09, // v3 = (v1 + v2) * 3 = 3 * 3 = 9
      0x05, // len = DataEnd - DataBeg = 0x0005 - 0x0000 = 5
  };
  auto& data{generator.GetData()};
  auto bin{data.os.rawbuf()};
  auto counter{0};
  for (const auto& c : bin) {
    std::cout << std::hex << std::uppercase << std::setw(2) << std::setfill('0')
              << (int)(unsigned char)c << " ";
    ASSERT_EQ(kAns[counter], (unsigned char)c);
    ++counter;
  }
  std::cout << std::dec << "\n";
}

TEST(AssemblerTest, GeneratorErrorCheck) {
  std::vector<std::string> srcs{kTestAsm, kTestFormla, kTestError};
  GBDev::AssemblerImpl::Tokenizer tokenizer;
  for (auto& src: srcs) {
    tokenizer.SetSource(src);
  }
  GBDev::AssemblerImpl::Parser parser{tokenizer.GetToken()};
  GBDev::AssemblerImpl::Generator generator{parser.GetNodes()};
  ASSERT_NE(0, generator.GetErrorCount());
  const auto errs{generator.GetErrors()};
  for (const auto& err: errs) {
    GBDev::ErrorMsgHelper::Output(err, srcs[err.cursor.src], &std::cout);
  }
}