#include <gtest/gtest.h>

#include <iostream>

#include "parser_impl.hpp"

#include "testasm.hpp"

TEST(AssemblerTest, AsmParserCheck) {
  std::string src{kTestAsm};
  GBDev::AssemblerImpl::Tokenizer tokenizer{src};
  GBDev::AssemblerImpl::Parser parser{tokenizer.GetToken()};
  using Node = typename GBDev::AssemblerImpl::Parser::Node;
  constexpr std::pair<Node::Kind, std::string_view> kAns[]{
    // line 1
    {Node::Kind::LABEL, "MemCopy"},
    // line 2
    {Node::Kind::OPCODE, "ld"},
    {Node::Kind::OPERAND, "a"},
    {Node::Kind::OPERAND, "["},
    {Node::Kind::ADDRESS, "bc"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::REGISTER, "bc"},
    {Node::Kind::IMMEDIATE, "a"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::REGISTER, "a"},
    // line 3
    {Node::Kind::OPCODE, "ldi"},
    {Node::Kind::OPERAND, "["},
    {Node::Kind::OPERAND, "a"},
    {Node::Kind::IMMEDIATE, "a"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::REGISTER, "a"},
    {Node::Kind::ADDRESS, "hl"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::REGISTER, "hl"},
    // line 4
    {Node::Kind::OPCODE, "inc"},
    {Node::Kind::OPERAND, "bc"},
    {Node::Kind::IMMEDIATE, "bc"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::REGISTER, "bc"},
    // line 5
    {Node::Kind::OPCODE, "dec"},
    {Node::Kind::OPERAND, "de"},
    {Node::Kind::IMMEDIATE, "de"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::REGISTER, "de"},
    // line 6
    {Node::Kind::OPCODE, "ld"},
    {Node::Kind::OPERAND, "a"},
    {Node::Kind::OPERAND, "d"},
    {Node::Kind::IMMEDIATE, "d"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::REGISTER, "d"},
    {Node::Kind::IMMEDIATE, "a"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::REGISTER, "a"},
    // line 7
    {Node::Kind::OPCODE, "or"},
    {Node::Kind::OPERAND, "a"},
    {Node::Kind::OPERAND, "e"},
    {Node::Kind::IMMEDIATE, "e"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::REGISTER, "e"},
    {Node::Kind::IMMEDIATE, "a"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::REGISTER, "a"},
    // line 8
    {Node::Kind::OPCODE, "jp"},
    {Node::Kind::OPERAND, "nz"},
    {Node::Kind::OPERAND, "MemCopy"},
    {Node::Kind::IMMEDIATE, "MemCopy"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::IDENT, "MemCopy"},
    {Node::Kind::IMMEDIATE, "nz"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::REGISTER, "nz"},
    // line 9
    {Node::Kind::OPCODE, "ret"},
  };
  auto nodes{parser.GetNodes()};
  int count{0};
  const auto func{[&](auto& node, const auto func){
    if (node == nullptr) return;
    ASSERT_EQ(kAns[count].first, node->kind) << count;
    ASSERT_EQ(kAns[count].second, node->token.str) << count;
    ++count;
    func(node->lhs, func);
    func(node->rhs, func);
  }};
  for (const auto& node: nodes) {
    GBDev::AssemblerImpl::Parser::PrintMermaid(node, &std::cout);
    func(node, func);
  }
}

TEST(AssemblerTest, FormulaParserCheck) {
  std::string src{kTestFormla};
  GBDev::AssemblerImpl::Tokenizer tokenizer{src};
  GBDev::AssemblerImpl::Parser parser{tokenizer.GetToken()};
  using Node = typename GBDev::AssemblerImpl::Parser::Node;
  constexpr std::pair<Node::Kind, std::string_view> kAns[]{
    // line 0
    {Node::Kind::OPCODE, "def"},
    {Node::Kind::OPERAND, "v1"},
    {Node::Kind::OPERAND, "1"},
    {Node::Kind::IMMEDIATE, "1"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::NUMBER, "1"},
    {Node::Kind::IMMEDIATE, "v1"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::IDENT, "v1"},
    // line 1
    {Node::Kind::OPCODE, "def"},
    {Node::Kind::OPERAND, "v2"},
    {Node::Kind::OPERAND, "2"},
    {Node::Kind::IMMEDIATE, "2"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::NUMBER, "2"},
    {Node::Kind::IMMEDIATE, "v2"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::IDENT, "v2"},
    // line 2
    {Node::Kind::OPCODE, "def"},
    {Node::Kind::OPERAND, "v3"},
    {Node::Kind::OPERAND, "("},
    {Node::Kind::IMMEDIATE, "("},
    {Node::Kind::FORMULA, ""},
    {Node::Kind::MUL, "*"},
    {Node::Kind::ADD, "+"},
    {Node::Kind::IDENT, "v1"},
    {Node::Kind::IDENT, "v2"},
    {Node::Kind::NUMBER, "3"},
    {Node::Kind::IMMEDIATE, "v3"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::IDENT, "v3"},
    // line 3
    {Node::Kind::OPCODE, "def"},
    {Node::Kind::OPERAND, "len"},
    {Node::Kind::OPERAND, "DataEnd"},
    {Node::Kind::IMMEDIATE, "DataEnd"},
    {Node::Kind::FORMULA, ""},
    {Node::Kind::SUB, "-"},
    {Node::Kind::IDENT, "DataEnd"},
    {Node::Kind::IDENT, "DataBeg"},
    {Node::Kind::IMMEDIATE, "len"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::IDENT, "len"},
    // line 4
    {Node::Kind::LABEL, "DataBeg"},
    // line 5
    {Node::Kind::OPCODE, "db"},
    {Node::Kind::OPERAND, "0x"},
    {Node::Kind::OPERAND, "0x"},
    {Node::Kind::OPERAND, "0x"},
    {Node::Kind::OPERAND, "0x"},
    {Node::Kind::OPERAND, "0x"},
    {Node::Kind::IMMEDIATE, "0x"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::NUMBER, "6F"},
    {Node::Kind::IMMEDIATE, "0x"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::NUMBER, "6C"},
    {Node::Kind::IMMEDIATE, "0x"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::NUMBER, "6C"},
    {Node::Kind::IMMEDIATE, "0x"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::NUMBER, "65"},
    {Node::Kind::IMMEDIATE, "0x"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::NUMBER, "48"},
    // line 6
    {Node::Kind::LABEL, "DataEnd"},
    // line 7
    {Node::Kind::OPCODE, "db"},
    {Node::Kind::OPERAND, "v1"},
    {Node::Kind::OPERAND, "v2"},
    {Node::Kind::OPERAND, "v3"},
    {Node::Kind::OPERAND, "len"},
    {Node::Kind::IMMEDIATE, "len"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::IDENT, "len"},
    {Node::Kind::IMMEDIATE, "v3"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::IDENT, "v3"},
    {Node::Kind::IMMEDIATE, "v2"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::IDENT, "v2"},
    {Node::Kind::IMMEDIATE, "v1"},
    {Node::Kind::VALUE, ""},
    {Node::Kind::IDENT, "v1"},
  };
  auto nodes{parser.GetNodes()};
  int count{0};
  const auto func{[&](auto& node, const auto func){
    if (node == nullptr) return;
    ASSERT_EQ(kAns[count].first, node->kind) << count;
    ASSERT_EQ(kAns[count].second, node->token.str) << count;
    ++count;
    func(node->lhs, func);
    func(node->rhs, func);
  }};
  for (const auto& node: nodes) {
    GBDev::AssemblerImpl::Parser::PrintMermaid(node, &std::cout);
    func(node, func);
  }
}

TEST(AssemblerTest, ParserErrorCheck) {
  std::vector<std::string> srcs{kTestAsm, kTestFormla, kTestError};
  GBDev::AssemblerImpl::Tokenizer tokenizer;
  for (auto& src: srcs) {
    tokenizer.SetSource(src);
  }
  GBDev::AssemblerImpl::Parser parser{tokenizer.GetToken()};
  ASSERT_NE(0, parser.GetErrorCount());
  const auto errs{parser.GetErrors()};
  for (const auto& err: errs) {
    GBDev::ErrorMsgHelper::Output(err, srcs[err.cursor.src], &std::cout);
  }
}