#pragma once

#include <list>
#include <ostream>
#include <string>

#include "textcursor.hpp"

namespace GBDev {

struct ErrorMsg {
  const std::string msg;
  const TextCursor cursor;

  ErrorMsg(std::string&& msg, const TextCursor& cursor = TextCursor{})
      : msg(std::move(msg)), cursor(cursor) {}
};

class ErrorMsgMgr {
 public:
  const std::list<ErrorMsg>& GetErrors() const { return errors_; }

  auto GetErrorCount() const { return errors_.size(); }

  void MergeErrors(ErrorMsgMgr& rhs) {
    auto comp{[](const ErrorMsg& lhs, const ErrorMsg& rhs) {
      return lhs.cursor.row != rhs.cursor.row ? lhs.cursor.row < rhs.cursor.row
                                              : lhs.cursor.col < rhs.cursor.col;
    }};
    errors_.merge(std::move(rhs.errors_), comp);
  }

  void ClearErrors() { errors_.clear(); }

 protected:
  ErrorMsgMgr() = default;
  ~ErrorMsgMgr() = default;
  std::list<ErrorMsg> errors_;
};

namespace ErrorMsgHelper {

void Output(const ErrorMsg& err, const std::string& src, std::ostream* os);

}  // namespace ErrorMsgHelper

}  // namespace GBDev