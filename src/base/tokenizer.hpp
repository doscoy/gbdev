#pragma once

#include <memory>
#include <string>
#include <string_view>
#include <deque>

#include "errormsg.hpp"
#include "textcursor.hpp"

namespace GBDev {

template <class KindType>
struct Token {
  using Kind = KindType;
  const Kind kind;
  const std::string str;
  const TextCursor cursor;

  Token(const Kind kind, std::string&& str, const TextCursor& cursor)
      : kind(kind),
        str(std::move(str)),
        cursor(cursor) {}
  
  Token() : Token(Kind{}, "", TextCursor{}) {}
};

template <class TokenizerImpl>
class TokenizerBase : public ErrorMsgMgr {
  friend TokenizerImpl;

 public:
  using Token = Token<typename TokenizerImpl::Kind>;
  using TokenContainer = std::deque<Token>;

  TokenizerBase() : impl_(*this), src_counter_(0) {}
  TokenizerBase(const std::string& src) : impl_(*this), src_counter_(0) {
    (void)SetSource(src);
  }
  ~TokenizerBase() {}

  int SetSource(const std::string& src) {
    impl_.Tokenize(src);
    return src_counter_++;
  }

  const TokenContainer& GetToken() const { return token_; }

  TokenContainer&& MoveToken() { return std::move(token_); }

  int GetSrcCount() const { return src_counter_; }

 private:

  TokenizerImpl impl_;
  int src_counter_;
  TokenContainer token_;
};

}  // namespace GBDev