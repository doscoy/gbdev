#pragma once

#include <cstdint>
#include <functional>
#include <iomanip>
#include <iostream>
#include <string_view>
#include <vector>

namespace Util {

template <class Type>
class BinOut {
  using size_type = decltype(sizeof(Type));

 public:
  BinOut() = delete;

  BinOut(const Type& value, size_type size)
      : value_(value), size_(std::min(size, sizeof(Type))) {}

  BinOut(const Type& value) : BinOut(value, sizeof(Type)) {}

 private:
  std::ostream& operator()(std::ostream& os) const {
    os.write(reinterpret_cast<const char*>(&value_), size_);
    return os;
  }

  friend std::ostream& operator<<(std::ostream& os, const BinOut& manip) {
    return manip(os);
  }

  const Type& value_;
  const size_type size_;
};

class binstreambuf : public std::streambuf {
  using si = std::ios;
  using sib = std::ios_base;

 public:
  using char_type = typename std::streambuf::char_type;
  using traits_type = typename std::streambuf::traits_type;
  using int_type = typename traits_type::int_type;
  using pos_type = typename traits_type::pos_type;
  using off_type = typename traits_type::off_type;
  using buffer_type = std::vector<char_type>;

  binstreambuf(sib::openmode wch = sib::in | sib::out) : hm_(0), mode_(wch) {
    init();
  }

  binstreambuf(const buffer_type::size_type reserve,
               sib::openmode wch = sib::in | sib::out)
      : hm_(0), mode_(wch) {
    buffer_.reserve(reserve);
    init();
  }

  binstreambuf(buffer_type&& buffer, sib::openmode wch = sib::in | sib::out)
      : buffer_(buffer), hm_(0), mode_(wch) {
    init();
  }

  ~binstreambuf() {}
  const buffer_type& rawbuf() const { return buffer_; }

  buffer_type&& movebuf() { return std::move(buffer_); }

  void movebuf(buffer_type&& buffer) {
    buffer_ = buffer;
    if (mode_ & sib::out) {
      fitoutputs();
    }
    if (mode_ & sib::in) {
      fitinputs();
    }
  }

  pos_type seekoff(off_type off, sib::seekdir way,
                   sib::openmode wch = sib::in | sib::out) override {
    if (hm_ < pptr()) hm_ = pptr();
    if ((wch & (sib::in | sib::out)) == 0) return pos_type(-1);
    if ((wch & (sib::in | sib::out)) == (sib::in | sib::out) && way == sib::cur)
      return pos_type(-1);
    const off_type hm = {hm_ == nullptr ? 0 : hm_ - buffer_.data()};
    off_type noff;
    switch (way) {
      case sib::beg:
        noff = 0;
        break;
      case sib::cur:
        if (wch & sib::in)
          noff = gptr() - eback();
        else
          noff = pptr() - pbase();
        break;
      case sib::end:
        noff = hm;
        break;
      default:
        return pos_type(-1);
    }
    noff += off;
    if (noff < 0 || hm < noff) return pos_type(-1);
    if (noff != 0) {
      if ((wch & sib::in) && gptr() == 0) return pos_type(-1);
      if ((wch & sib::out) && pptr() == 0) return pos_type(-1);
    }
    if (wch & sib::in) setg(eback(), eback() + noff, hm_);
    if (wch & sib::out) {
      setp(pbase(), epptr());
      pbump(noff);
    }
    return pos_type(noff);
  }

  pos_type seekpos(pos_type sp,
                   sib::openmode wch = sib::in | sib::out) override {
    return seekoff(sp, sib::beg, wch);
  }

  int_type overflow(int_type c = traits_type::eof()) override {
    if (traits_type::eq_int_type(c, traits_type::eof())) {
      return traits_type::not_eof(c);
    }
    if (pptr() == epptr()) {
      if (!(mode_ & sib::out)) return traits_type::eof();
      fitoutputs();
    }
    hm_ = std::max(pptr() + 1, hm_);
    if (mode_ & sib::in) {
      fitinputs();
    }
    return sputc(traits_type::to_char_type(c));
  }

 private:
  void init() {
    auto* beg{buffer_.data()};
    if (mode_ & sib::out) {
      setp(beg, beg + buffer_.size());
    }
    if (mode_ & sib::in) {
      setg(beg, beg, hm_);
    }
  }

  void fitoutputs() {
    const auto nout{pptr() - pbase()};
    const auto hm{hm_ - pbase()};
    buffer_.resize(buffer_.size() + 1);
    auto* beg{buffer_.data()};
    setp(beg, beg + buffer_.size());
    pbump(nout);
    hm_ = pbase() + hm;
  }

  void fitinputs() {
    const auto ninp{gptr() - eback()};
    auto* beg{buffer_.data()};
    setg(beg, beg + ninp, hm_);
  }

  std::vector<char_type> buffer_;
  mutable char_type* hm_;
  sib::openmode mode_;
};

class binstream : public std::iostream {
 public:
  using buffer_type = typename binstreambuf::buffer_type;

  binstream() : std::iostream(&buffer_), buffer_(){};

  binstream(const binstreambuf::buffer_type::size_type reserve)
      : std::iostream(&buffer_), buffer_(reserve){};

  binstream(binstreambuf::buffer_type&& buffer)
      : std::iostream(&buffer_), buffer_(std::move(buffer)){};

  binstream(binstream&& rhs)
      : std::iostream(&buffer_), buffer_(std::move(rhs.buffer_)){};

  const binstreambuf::buffer_type& rawbuf() const { return buffer_.rawbuf(); }
  binstreambuf::buffer_type&& movebuf() { return buffer_.movebuf(); }
  void movebuf(binstreambuf::buffer_type&& buffer) {
    buffer_.movebuf(std::forward<binstreambuf::buffer_type>(buffer));
  }

 private:
  binstreambuf buffer_;
};

template <class IntType>
constexpr IntType FillCharInt(const char c,
                              const decltype(sizeof(IntType)) pos = 0) {
  return pos < sizeof(IntType)
             ? IntType{c << (8 * pos)} | FillCharInt<IntType>(c, pos + 1)
             : 0;
}

template <class Type>
constexpr Type Pow(const int base, const int n) {
  return n == 0 ? 1 : base * Pow<Type>(base, n - 1);
}

template <class Type>
constexpr Type EncodeBase36(const std::string_view& str, const int pos = 0) {
  return pos >= static_cast<int>(str.length()) ? 0
         : str[pos] >= '0' && str[pos] <= '9'
             ? (str[pos] - '0') * Pow<Type>(36, str.length() - 1 - pos) +
                   EncodeBase36<Type>(str, pos + 1)
         : str[pos] >= 'a' && str[pos] <= 'z'
             ? (str[pos] - 'a' + 10) * Pow<Type>(36, str.length() - 1 - pos) +
                   EncodeBase36<Type>(str, pos + 1)
         : str[pos] >= 'A' && str[pos] <= 'Z'
             ? (str[pos] - 'A' + 10) * Pow<Type>(36, str.length() - 1 - pos) +
                   EncodeBase36<Type>(str, pos + 1)
             : EncodeBase36<Type>(str, pos + 1);
}

template <class Type>
constexpr int Base36Digit(const Type base36, const int counter = 0) {
  return base36 / 36 == 0 ? counter + 1 : Base36Digit(base36 / 36, counter + 1);
}

template <class Type>
std::string DecodeBase36(const Type base36, const bool upper = false) {
  const int digit{Base36Digit(base36)};
  std::string tmp;
  tmp.resize(digit);
  int counter{0};
  auto accum{base36};
  while (accum != 0) {
    const auto mod{accum % 36};
    const char c{mod < 10   ? static_cast<char>(mod + '0')
                 : mod < 36 ? upper ? static_cast<char>((mod - 10) + 'A')
                                    : static_cast<char>((mod - 10) + 'a')
                            : '!'};
    tmp[digit - counter - 1] = c;
    accum /= 36;
    ++counter;
  }
  return tmp;
}

}  // namespace Util