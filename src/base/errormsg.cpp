#include "errormsg.hpp"

#include <algorithm>

namespace GBDev {

namespace ErrorMsgHelper {

namespace {

std::string PickLine(const TextCursor& cursor, const std::string& src) {
  const auto beg_tmp{src.rfind('\n', cursor.pos - 1)};
  const auto end_tmp{src.find('\n', cursor.pos)};
  const auto beg_pos{beg_tmp != std::string::npos ? beg_tmp + 1 : 0};
  const auto end_pos{end_tmp != std::string::npos ? end_tmp : src.size()};
  return src.substr(beg_pos, end_pos - beg_pos);
}

std::string HighlightPos(const TextCursor& cursor, const std::string& line) {
  std::string str{};
  if (cursor.col < 0 || cursor.len < 0) {
    str.resize(line.size());
    std::fill_n(str.data(), str.size(), '~');
  } else {
    str.resize(cursor.col + cursor.len);
    auto read_itr{line.begin()};
    auto write_itr{str.begin()};
    for (int i = 0; i < cursor.col; ++i, ++read_itr, ++write_itr) {
      *write_itr = *read_itr == '\t' ? '\t' : ' ';
    }
    for (int i = 0; i < cursor.len; ++i, ++read_itr, ++write_itr) {
      *write_itr = '~';
    }
  }
  return str;
}

}  // namespace

void Output(const ErrorMsg& err, const std::string& src, std::ostream* os) {
  *os << err.cursor.row + 1 << ":" << err.cursor.col + 1 << ": ";
  *os << err.msg << "\n";
  std::string line{PickLine(err.cursor, src)};
  *os << line << "\n";
  *os << HighlightPos(err.cursor, line) << "\n";
}

}  // namespace ErrorMsgHelper

}  // namespace GBDev