#pragma once

namespace GBDev {

struct TextCursor {
  int src, pos, len, row, col;

  TextCursor(const int src, const int pos, const int len, const int row,
             const int col)
      : src(src), pos(pos), len(len), row(row), col(col) {}
  
  TextCursor() : TextCursor(-1, -1, -1, -1, -1) {}
};

}