#pragma once

#include <deque>
#include <iostream>
#include <memory>

#include "errormsg.hpp"
#include "myutil.hpp"

namespace GBDev {

template <class GeneratorImpl>
class GeneratorBase : public ErrorMsgMgr {
  friend GeneratorImpl;

 public:
  using Parser = typename GeneratorImpl::Parser;
  using Node = typename Parser::Node;
  using NodePtr = typename Parser::NodePtr;
  using NodeContainer = typename Parser::NodeContainer;
  using Data = typename GeneratorImpl::Data;

  GeneratorBase() : impl_(*this) {}

  GeneratorBase(const NodeContainer& nodes) : impl_(*this) { SetNodes(nodes); }

  ~GeneratorBase() {}

  void SetNodes(const NodeContainer& nodes) { impl_.Generate(nodes); }

  const Data& GetData() const { return data_; }

  Data&& MoveData() { return std::move(data_); }

 private:
  GeneratorImpl impl_;
  Data data_;
};

}  // namespace GBDev