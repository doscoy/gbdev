#pragma once

#include <deque>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>

#include "errormsg.hpp"
#include "myutil.hpp"

namespace GBDev {

template <class KindType, class Token>
struct Node {
  using Kind = KindType;
  using Pointer = std::shared_ptr<Node<Kind, Token>>;
  const Kind kind;
  const Pointer lhs;
  const Pointer rhs;
  const Token token;
  const int value;

  Node(const Kind kind, Pointer&& lhs, Pointer&& rhs, Token&& token,
       const int value = 0)
      : kind{kind}, lhs{lhs}, rhs{rhs}, token{token}, value{value} {}

  Node(const Kind kind, Pointer&& lhs, Pointer&& rhs, const int value = 0)
      : Node(kind, std::move(lhs), std::move(rhs), Token{}, value) {}

  Node(const Kind kind, Token&& token, const int value = 0)
      : Node(kind, nullptr, nullptr, std::move(token), value) {}

  Node(const Kind kind, const int value = 0)
      : Node(kind, nullptr, nullptr, value) {}

  template <class... Args>
  static Pointer MakePtr(Args&&... args) {
    return Pointer{new Node<Kind, Token>{std::forward<Args>(args)...}};
  }
};

template <class ParserImpl>
class ParserBase : public ErrorMsgMgr {
  friend ParserImpl;

 public:
  using Token = typename ParserImpl::Tokenizer::Token;
  using TokenContainer = typename ParserImpl::Tokenizer::TokenContainer;
  using Node = Node<typename ParserImpl::Kind, Token>;
  using NodePtr = typename Node::Pointer;
  using NodeContainer = std::deque<NodePtr>;

  ParserBase() : impl_(*this) {}
  ParserBase(const TokenContainer& token) : impl_(*this) { SetToken(token); }
  ~ParserBase() {}

  void SetToken(const TokenContainer& token) { impl_.Parse(token); }

  const NodeContainer& GetNodes() const { return nodes_; }

  NodeContainer&& MoveNodes() { return std::move(nodes_); }

  static void PrintMermaid(const typename Node::Pointer node,
                           std::ostream* os) {
    if (node == nullptr) return;
    std::string name_buff{"item"};
    std::ostringstream node_desc;
    std::ostringstream path_desc;
    auto recursive{[&node_desc, &path_desc](auto node, auto name, auto tag,
                                            auto func) -> void {
      node_desc << name << "[" << tag << "<br>kind: " << (int)node->kind
                << "<br>value: " << node->value
                << "<br>str: " << node->token.str << "]\n";
      if (node->lhs != nullptr) {
        const std::string tmp{name + "lhs"};
        path_desc << name << "-->" << tmp << "\n";
        func(node->lhs, tmp, "lhs", func);
      }
      if (node->rhs != nullptr) {
        const std::string tmp{name + "rhs"};
        path_desc << name << "-->" << tmp << "\n";
        func(node->rhs, tmp, "rhs", func);
      }
    }};
    recursive(node, name_buff, "root", recursive);
    *os << "graph TD\n" << node_desc.str() << path_desc.str() << "\n";
  }

 private:
  ParserImpl impl_;
  NodeContainer nodes_;
};

}  // namespace GBDev