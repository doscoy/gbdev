#include <emscripten/bind.h>
#include <emscripten/emscripten.h>
#include <emscripten/val.h>
#include <string>
#include <vector>
#include <sstream>
#include "assembler.hpp"

static GBDev::Assembler assembler;

int Input(std::string src) {
  return assembler.Input(src);
}

int Assemble() {
  return assembler.Assemble();
}

std::vector<char> GetBinary() {
  return assembler.GetRom();
}

std::vector<std::string> GetErrorList(const emscripten::val src_jsarray) {
  const auto srcs{emscripten::vecFromJSArray<std::string>(src_jsarray)};
  const auto errs{assembler.GetErrors()};
  std::vector<std::string> buf;
  for (const auto& err: errs) {
    std::ostringstream oss;
    GBDev::ErrorMsgHelper::Output(err, srcs[err.cursor.src], &oss);
    buf.emplace_back(oss.str());
  }
  return buf;
}

void Reset() {
  assembler.Reset();
}

EMSCRIPTEN_BINDINGS(GBasm) {
  emscripten::function("Input", &Input);
  emscripten::function("Assemble", &Assemble);
  emscripten::function("GetBinary", &GetBinary);
  emscripten::function("GetErrorList", &GetErrorList);
  emscripten::function("Reset", &Reset);
  emscripten::register_vector<char>("vector<char>");
  emscripten::register_vector<std::string>("vector<string>");
}