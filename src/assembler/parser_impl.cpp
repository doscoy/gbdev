#include "parser_impl.hpp"

namespace GBDev {

namespace AssemblerImpl {

void ParserImpl::Parse(const Tokenizer::TokenContainer& token) {
  auto itr{token.begin()};
  const auto end{token.end()};
  while (itr < end) {
    auto node{Statement(itr)};
    if (node != nullptr) {
      base_.nodes_.emplace_back(std::move(node));
    }
  }
}

ParserImpl::Node::Pointer ParserImpl::Statement(TokenItr& itr) {
  auto node{Label(itr)};
  if (itr->kind == Token::Kind::EOL) {
    ++itr;
  } else {
    base_.errors_.emplace_back("error: parser got stuck", itr->cursor);
    while (itr->kind != Token::Kind::EOL) {
      ++itr;
    }
  }
  return node;
}

ParserImpl::Node::Pointer ParserImpl::Label(TokenItr& itr) {
  if (itr->kind == Token::Kind::STRING && (itr + 1)->str == ":") {
    auto token{*(itr++)};
    return Node::MakePtr(Node::Kind::LABEL, Instruct(++itr), nullptr,
                         std::move(token));
  } else if (itr->str == ":") {
    auto token{*itr};
    return Node::MakePtr(Node::Kind::LABEL, Instruct(++itr), nullptr,
                         std::move(token));
  }
  return Instruct(itr);
}

ParserImpl::Node::Pointer ParserImpl::Instruct(TokenItr& itr) {
  if (itr->kind == Token::Kind::STRING) {
    auto token{*itr};
    return Node::MakePtr(Node::Kind::OPCODE, Operand(++itr), nullptr,
                         std::move(token), std::stoi(token.str, nullptr, 36));
  } else if (itr->kind == Token::Kind::EOL) {
    return nullptr;
  } else {
    base_.errors_.emplace_back("error: invalid token", itr->cursor);
    return nullptr;
  }
}

ParserImpl::Node::Pointer ParserImpl::Operand(TokenItr& itr) {
  auto token{*itr};
  auto rhs{OperandType(itr)};
  if (rhs != nullptr) {
    auto lhs{itr->str == "," ? Operand(++itr) : nullptr};
    return Node::MakePtr(Node::Kind::OPERAND, std::move(lhs), std::move(rhs),
                         std::move(token));
  } else {
    return nullptr;
  }
}

ParserImpl::Node::Pointer ParserImpl::OperandType(TokenItr& itr) {
  Node::Kind kind;
  if (itr->str == "[") {
    ++itr;
    kind = Node::Kind::ADDRESS;
  } else {
    kind = Node::Kind::IMMEDIATE;
  }
  auto token{*itr};
  auto node{Expr(itr)};
  if (node != nullptr) {
    const bool flag{node->kind == Node::Kind::NUMBER ||
                    node->kind == Node::Kind::IDENT ||
                    node->kind == Node::Kind::REGISTER};
    auto type{flag ? Node::MakePtr(Node::Kind::VALUE)
                   : Node::MakePtr(Node::Kind::FORMULA)};
    node =
        Node::MakePtr(kind, std::move(type), std::move(node), std::move(token));
    if (itr->str == "]") {
      ++itr;
    } else if (kind == Node::Kind::ADDRESS) {
      base_.errors_.emplace_back("error: expected \"]\"", itr->cursor);
    }
  }
  return node;
}

ParserImpl::Node::Pointer ParserImpl::Expr(TokenItr& itr) {
  return Bitwise(itr);
}

ParserImpl::Node::Pointer ParserImpl::Bitwise(TokenItr& itr) {
  const auto base{
      [](const std::string_view str, const Node::Kind kind, const auto func) {
        return [str, kind, func](TokenItr& itr) {
          Node::Pointer node{func(itr)};
          while (true) {
            auto token{*itr};
            if (itr->str == str) {
              node = Node::MakePtr(kind, std::move(node), func(++itr),
                                   std::move(token));
            } else {
              return node;
            }
          }
        };
      }};
  const auto wrap{[this](TokenItr& itr) { return this->Shift(itr); }};
  const auto bit_and{base("&", Node::Kind::BITAND, wrap)};
  const auto bit_xor{base("^", Node::Kind::BITXOR, bit_and)};
  const auto bit_or{base("|", Node::Kind::BITOR, bit_xor)};
  return bit_or(itr);
}

ParserImpl::Node::Pointer ParserImpl::Shift(TokenItr& itr) {
  auto node{Add(itr)};
  while (true) {
    auto token{*itr};
    if (itr->str == "<<") {
      node = Node::MakePtr(Node::Kind::LSHIFT, std::move(node), Add(++itr),
                           std::move(token));
    } else if (itr->str == ">>") {
      node = Node::MakePtr(Node::Kind::RSHIFT, std::move(node), Add(++itr),
                           std::move(token));
    } else {
      return node;
    }
  }
}

ParserImpl::Node::Pointer ParserImpl::Add(TokenItr& itr) {
  auto node{Mul(itr)};
  while (true) {
    auto token{*itr};
    if (itr->str == "+") {
      node = Node::MakePtr(Node::Kind::ADD, std::move(node), Mul(++itr),
                           std::move(token));
    } else if (itr->str == "-") {
      node = Node::MakePtr(Node::Kind::SUB, std::move(node), Mul(++itr),
                           std::move(token));
    } else {
      return node;
    }
  }
}

ParserImpl::Node::Pointer ParserImpl::Mul(TokenItr& itr) {
  auto node{Unary(itr)};
  while (true) {
    auto token{*itr};
    if (itr->str == "*") {
      node = Node::MakePtr(Node::Kind::MUL, std::move(node), Unary(++itr),
                           std::move(token));
    } else if (itr->str == "/") {
      node = Node::MakePtr(Node::Kind::DIV, std::move(node), Unary(++itr),
                           std::move(token));
    } else if (itr->str == "%") {
      node = Node::MakePtr(Node::Kind::MOD, std::move(node), Unary(++itr),
                           std::move(token));
    } else {
      return node;
    }
  }
}

ParserImpl::Node::Pointer ParserImpl::Unary(TokenItr& itr) {
  if (itr->str == "+") {
    return Primary(++itr);
  } else if (itr->str == "-") {
    auto token{*itr};
    return Node::MakePtr(Node::Kind::MOD, Node::MakePtr(Node::Kind::NUMBER),
                         Primary(++itr), std::move(token));
  }
  return Primary(itr);
}

ParserImpl::Node::Pointer ParserImpl::Primary(TokenItr& itr) {
  if (itr->str == "(") {
    auto node{Expr(++itr)};
    if (itr->str == ")") {
      ++itr;
    } else {
      base_.errors_.emplace_back("error: expected \")\"", itr->cursor);
    }
    return node;
  }
  if (itr->kind == Token::Kind::PREHEX) {
    return Number(++itr, 16);
  }
  if (itr->kind == Token::Kind::PREBIN) {
    return Number(++itr, 2);
  }
  if (itr->kind == Token::Kind::NUMBER) {
    return Number(itr, 10);
  }
  if (itr->kind == Token::Kind::STRING) {
    return String(itr);
  }
  if (itr->kind == Token::Kind::EOL) {
    return nullptr;
  }
  base_.errors_.emplace_back("error: unexpected token", itr->cursor);
  return nullptr;
}

ParserImpl::Node::Pointer ParserImpl::Number(TokenItr& itr, const int base) {
  auto token{*(itr++)};
  return Node::MakePtr(Node::Kind::NUMBER, std::move(token),
                       std::stoi(token.str, nullptr, base));
}

ParserImpl::Node::Pointer ParserImpl::String(TokenItr& itr) {
  constexpr std::string_view kRegList{
      "a f b c d e f h l af bc de hl sp z nz nc "
      "A F B C D E F H L AF BC DE HL SP Z NZ NC"};
  auto token{*itr};
  const auto pos{kRegList.find(token.str)};
  ++itr;
  if (pos != std::string_view::npos) {
    return Node::MakePtr(Node::Kind::REGISTER, std::move(token),
                         std::stoi(token.str, nullptr, 36));
  } else {
    return Node::MakePtr(Node::Kind::IDENT, std::move(token));
  }
}

}  // namespace AssemblerImpl

}  // namespace GBDev