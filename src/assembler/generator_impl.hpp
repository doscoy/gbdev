#pragma once

#include <unordered_map>

#include "generator.hpp"
#include "myutil.hpp"
#include "parser_impl.hpp"

namespace GBDev {

namespace AssemblerImpl {

class GeneratorImpl {
  // boilerplate {
  struct Data {
    Util::binstream os;
    std::unordered_map<std::string, Parser::Node::Pointer> defs;
    std::deque<std::tuple<Parser::Node::Pointer, int, int>> refs;
  };
  using Parser = Parser;
  using Base = GeneratorBase<GeneratorImpl>;
  friend Base;
  GeneratorImpl(Base& base) : base_(base) {}
  void Generate(const Parser::NodeContainer& nodes);
  Base& base_;
  // } boilerplate

  using Token = Parser::Token;
  using Node = Parser::Node;

  void ResolvingReference();
  void Opcode(const Node::Pointer& opcode);

  void OP_xx(const Node::Pointer& opcode, const int num);

  // OP_xx_ImmRegX8_xx
  void OP_xx_ImmRegX8(const Node::Pointer& operand_2, const int reg,
                      const int num);
  void OP_xx_ImmRegX8_Imm(const Node::Pointer& operand_2, const int reg,
                          const int num);
  void OP_xx_ImmRegX8_ImmReg(const Node::Pointer& operand_2, const int reg);
  void OP_xx_ImmRegX8_Addr(const Node::Pointer& operand_2, const int reg);

  // OP_xx_ImmRegA
  void OP_xx_ImmRegA_XX(const Node::Pointer& opcode, const int reg,
                        const int num);

  // OP_ld
  void OP_ld(const Node::Pointer& opcode);
  void OP_ld_Imm(const Node::Pointer& operand_1,
                 const Node::Pointer& operand_2);
  void OP_ld_ImmRegA(const Node::Pointer& operand_2);
  void OP_ld_ImmRegA_Addr(const Node::Pointer& operand_2);
  void OP_ld_ImmRegA_AddrReg(const Node::Pointer& operand_2);
  void OP_ld_ImmRegX16(const Node::Pointer& operand_2, const int num);
  void OP_ld_ImmRegSP(const Node::Pointer& operand_2);
  void OP_ld_Addr(const Node::Pointer& operand_1,
                  const Node::Pointer& operand_2);
  void OP_ld_AddrReg(const Node::Pointer& operand_1,
                     const Node::Pointer& operand_2);
  void OP_ld_AddrRegX(const Node::Pointer& operand_2, const int num);
  void OP_ld_AddrRegHL(const Node::Pointer& operand_2);
  void OP_ld_AddrRegHL_ImmReg(const Node::Pointer& operand_2);
  void OP_ldi(const Node::Pointer& opcode);
  void OP_ldd(const Node::Pointer& opcode);
  void OP_ldh(const Node::Pointer& opcode);

  void OP_push_Or_pop(const Node::Pointer& opcode, const int base);

  void OP_add(const Node::Pointer& opcode);
  void OP_add_hl(const Node::Pointer& operand_2);

  void OP_inc(const Node::Pointer& opcode);
  void OP_inc_Imm(const Node::Pointer& operand);
  void OP_inc_Addr(const Node::Pointer& operand);

  void OP_dec(const Node::Pointer& opcode);
  void OP_dec_Imm(const Node::Pointer& operand);
  void OP_dec_Addr(const Node::Pointer& operand);

  void OP_cb(const Node::Pointer& opcode, const int base);
  void OP_cb_Imm(const Node::Pointer& operand, const int base);
  void OP_cb_Addr(const Node::Pointer& operand, const int base);
  void OP_cb_bit(const Node::Pointer& opcode, const int base);

  void OP_stop(const Node::Pointer& opcode);

  void OP_jp(const Node::Pointer& opcode);
  void OP_jp_xx(const Node::Pointer& operand);
  void OP_jp_xx_xx(const Node::Pointer& operand_1,
                   const Node::Pointer& operand_2);

  void OP_jr(const Node::Pointer& opcode);
  void OP_jr_xx(const Node::Pointer& operand);
  void OP_jr_xx_xx(const Node::Pointer& operand_1,
                   const Node::Pointer& operand_2);

  void OP_call(const Node::Pointer& opcode);
  void OP_call_xx(const Node::Pointer& operand);
  void OP_call_xx_xx(const Node::Pointer& operand_1,
                     const Node::Pointer& operand_2);

  void OP_ret(const Node::Pointer& opcode);
  void OP_ret_xx(const Node::Pointer& operand);

  void OP_rst(const Node::Pointer& opcode);

  void OP_db(const Node::Pointer& opcode);
  void OP_ds(const Node::Pointer& opcode);
  void OP_dp(const Node::Pointer& opcode);
  void OP_def(const Node::Pointer& opcode);
};

using Generator = GeneratorBase<GeneratorImpl>;

}  // namespace AssemblerImpl

}  // namespace GBDev