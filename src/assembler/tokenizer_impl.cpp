#include "tokenizer_impl.hpp"

namespace GBDev {

namespace AssemblerImpl {

class TokenizerImpl::CursorMgr {
  using StringType = std::string;

 public:
  CursorMgr(const StringType& src, const int id)
      : end(src.end()),
        itr(src.begin()),
        src_(src),
        id_(id),
        beg_(itr),
        row_beg_(itr),
        row_cnt_(0) {}

  auto GetStr(const int len) const { return src_.substr(itr - beg_, len); }

  auto GetCursor(const int len) const {
    return TextCursor{id_, static_cast<int>(itr - beg_), len, row_cnt_,
                      static_cast<int>(itr - row_beg_)};
  }

  void LineFeed() {
    ++row_cnt_;
    row_beg_ = itr;
  }

 public:
  const StringType::const_iterator end;
  StringType::const_iterator itr;

 private:
  const StringType& src_;
  const int id_;
  const StringType::const_iterator beg_;
  StringType::const_iterator row_beg_;
  int row_cnt_;
};

void TokenizerImpl::Tokenize(const std::string& src) {
  CursorMgr cursor{src, base_.src_counter_};
  while (cursor.itr < cursor.end) {
    if (CheckSkipCharacter(cursor)) {
      continue;
    }
    if (CheckComment(cursor)) {
      continue;
    }
    if (CheckEOLToken(cursor)) {
      continue;
    }
    if (CheckNumberToken(cursor)) {
      continue;
    }
    if (CheckDoubleSymbolToken(cursor)) {
      continue;
    }
    if (CheckSingleSymbolToken(cursor)) {
      continue;
    }
    if (CheckStringToken(cursor)) {
      continue;
    }
    HandlingInvalidCharacter(cursor);
  }
  base_.token_.emplace_back(Token::Kind::EOL, "", cursor.GetCursor(1));
}

template <class Lambda>
bool TokenizerImpl::StoreHelper(const Lambda& func, const Token::Kind kind,
                                CursorMgr& cursor) {
  auto sub_itr{cursor.itr};
  while (func(sub_itr) && sub_itr < cursor.end) {
    ++sub_itr;
  }
  if (sub_itr != cursor.itr) {
    const auto len{sub_itr - cursor.itr};
    base_.token_.emplace_back(kind, cursor.GetStr(len), cursor.GetCursor(len));
    cursor.itr += len;
    return true;
  } else {
    return false;
  }
}

bool TokenizerImpl::CheckSkipCharacter(CursorMgr& cursor) {
  if (*cursor.itr == ' ' || *cursor.itr == '\t') {
    ++cursor.itr;
    return true;
  } else {
    return false;
  }
}

bool TokenizerImpl::CheckComment(CursorMgr& cursor) {
  if (*cursor.itr == ';') {
    while (cursor.itr < cursor.end) {
      ++cursor.itr;
      if (CheckEOLToken(cursor)) {
        break;
      }
    }
    return true;
  } else {
    return false;
  }
}

bool TokenizerImpl::CheckEOLToken(CursorMgr& cursor) {
  if (*cursor.itr == '\n') {
    if (base_.token_.size() != 0 &&
        base_.token_.back().kind != Token::Kind::EOL) {
      base_.token_.emplace_back(Token::Kind::EOL, cursor.GetStr(1),
                                cursor.GetCursor(1));
    }
    while (*cursor.itr == '\n') {
      ++cursor.itr;
      cursor.LineFeed();
    }
    return true;
  } else {
    return false;
  }
}

bool TokenizerImpl::CheckNumberToken(CursorMgr& cursor) {
  using Itr = decltype(cursor.itr);
  if (*cursor.itr == '0') {
    const auto next{*(cursor.itr + 1)};
    if (next == 'x' || next == 'X') {
      auto func = [](Itr& itr) -> bool {
        return (*itr >= '0' && *itr <= '9') || (*itr >= 'a' && *itr <= 'f') ||
               (*itr >= 'A' && *itr <= 'F');
      };
      base_.token_.emplace_back(Token::Kind::PREHEX, cursor.GetStr(2),
                                cursor.GetCursor(2));
      cursor.itr += 2;
      return StoreHelper(func, Token::Kind::NUMBER, cursor);
    } else if (next == 'b' || next == 'B') {
      auto func = [](Itr& itr) -> bool { return *itr == '0' || *itr == '1'; };
      base_.token_.emplace_back(Token::Kind::PREBIN, cursor.GetStr(2),
                                cursor.GetCursor(2));
      cursor.itr += 2;
      return StoreHelper(func, Token::Kind::NUMBER, cursor);
    }
  }
  auto func{[](Itr& itr) -> bool { return *itr >= '0' && *itr <= '9'; }};
  return StoreHelper(func, Token::Kind::NUMBER, cursor);
}

bool TokenizerImpl::CheckDoubleSymbolToken(CursorMgr& cursor) {
  constexpr std::string_view kSymbolList{"<< >>"};
  std::string buf{cursor.GetStr(2)};
  if (kSymbolList.find(buf) != kSymbolList.npos) {
    base_.token_.emplace_back(Token::Kind::SYMBOL, cursor.GetStr(2),
                              cursor.GetCursor(2));
    cursor.itr += 2;
    return true;
  } else {
    return false;
  }
}

bool TokenizerImpl::CheckSingleSymbolToken(CursorMgr& cursor) {
  constexpr std::string_view kSymbolList{":,[];()*/%+-&^|"};
  if (kSymbolList.find(*cursor.itr) != kSymbolList.npos) {
    base_.token_.emplace_back(Token::Kind::SYMBOL, cursor.GetStr(1),
                              cursor.GetCursor(1));
    ++cursor.itr;
    return true;
  } else {
    return false;
  }
}

bool TokenizerImpl::CheckStringToken(CursorMgr& cursor) {
  auto func{[](std::string::const_iterator& itr) {
    const bool lower_str{*itr >= 'a' && *itr <= 'z'};
    const bool upper_str{*itr >= 'A' && *itr <= 'Z'};
    const bool numbr_str{*itr >= '0' && *itr <= '9'};
    const bool under_str{*itr == '_'};
    return lower_str || upper_str || numbr_str || under_str;
  }};
  return StoreHelper(func, Token::Kind::STRING, cursor);
}

void TokenizerImpl::HandlingInvalidCharacter(CursorMgr& cursor) {
  base_.errors_.emplace_back("error: invalid character", cursor.GetCursor(1));
  ++cursor.itr;
}

}  // namespace AssemblerImpl

}  // namespace GBDev