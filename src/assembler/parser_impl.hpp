#pragma once

#include "tokenizer_impl.hpp"
#include "parser.hpp"

namespace GBDev {

namespace AssemblerImpl {

class ParserImpl {
  // boilerplate {
  enum class Kind {
    UNKNOW,
    LABEL,
    OPCODE,
    OPERAND,
    ADDRESS,
    IMMEDIATE,
    VALUE,
    FORMULA,
    ASSIGN,
    BITOR,
    BITXOR,
    BITAND,
    LSHIFT,
    RSHIFT,
    ADD,
    SUB,
    MUL,
    DIV,
    MOD,
    NUMBER,
    REGISTER,
    IDENT
  };
  using Tokenizer = Tokenizer;
  using Base = ParserBase<ParserImpl>;
  friend Base;
  ParserImpl(Base& base) : base_(base) {}
  void Parse(const Tokenizer::TokenContainer& token);
  Base& base_;
  // } boilerplate

  using Token = typename Tokenizer::Token;
  using TokenItr = typename Tokenizer::TokenContainer::const_iterator;
  using Node = Node<Kind, Token>;

  Node::Pointer Statement(TokenItr& itr);
  Node::Pointer Label(TokenItr& itr);
  Node::Pointer Instruct(TokenItr& itr);
  Node::Pointer Operand(TokenItr& itr);
  Node::Pointer OperandType(TokenItr& itr);
  Node::Pointer Expr(TokenItr& itr);
  Node::Pointer Bitwise(TokenItr& itr);
  Node::Pointer Shift(TokenItr& itr);
  Node::Pointer Add(TokenItr& itr);
  Node::Pointer Mul(TokenItr& itr);
  Node::Pointer Unary(TokenItr& itr);
  Node::Pointer Primary(TokenItr& itr);
  Node::Pointer Number(TokenItr& itr, const int base);
  Node::Pointer String(TokenItr& itr);
};
using Parser = ParserBase<ParserImpl>;

}

}