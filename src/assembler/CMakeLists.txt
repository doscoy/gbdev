add_library(assembler
  assembler.cpp
  tokenizer_impl.cpp
  parser_impl.cpp
  generator_impl.cpp
)

target_link_libraries(assembler base)

target_include_directories(assembler PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

target_compile_options(assembler
  PRIVATE
    -Wall
    -Wextra
    -pedantic-errors
)

add_executable(GBasm main.cpp)

target_link_libraries(GBasm assembler)

target_compile_options(GBasm
  PRIVATE
    -Wall
    -Wextra
    -pedantic-errors
)