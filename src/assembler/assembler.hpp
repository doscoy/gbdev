#pragma once

#include <cstdint>
#include <filesystem>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

#include "errormsg.hpp"
#include "generator_impl.hpp"
#include "parser_impl.hpp"
#include "tokenizer_impl.hpp"

namespace GBDev {

class Assembler : public ErrorMsgMgr {
  using Tokenizer = AssemblerImpl::Tokenizer;
  using Parser = AssemblerImpl::Parser;
  using Generator = AssemblerImpl::Generator;
  using Data = Generator::Data;

 public:
  Assembler();
  ~Assembler();

  int Input(const std::string& source);
  int Input(const std::filesystem::path path);
  int Input(const std::istream& is);

  int Assemble();
  int Assemble(std::ostream* token_log_os, std::ostream* node_log_os,
               std::ostream* data_log_os);
  int Assemble(std::ostream* log_os);

  Data&& MoveRawData();

  Data MoveRomData();

  std::vector<char>&& GetRawBinary();

  std::vector<char> GetRom();

  void Reset();

 private:
  void AssembleImpl();

  std::unique_ptr<Tokenizer> tokenizer_;
  std::unique_ptr<Parser> parser_;
  std::unique_ptr<Generator> generator_;
};

std::uint8_t Assemble(const std::string& src);

}  // namespace GBDev