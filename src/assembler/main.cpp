#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "assembler.hpp"

class IOMgr {
 public:
  IOMgr(int argc, char** argv) : is_(nullptr), os_(nullptr) {
    if (argc == 1) {
      std::stringstream* ssp{new std::stringstream{}};
      *ssp << std::cin.rdbuf();
      is_ = ssp;
      os_ = &std::cout;
      enable_ = true;
    } else {
      enable_ = InitIO(argv);
    }
  }
  ~IOMgr() {
    if (is_ != &std::cin && is_ != nullptr) {
      delete is_;
    }
    if (os_ != &std::cout && os_ != nullptr) {
      delete os_;
    }
  }

  bool Enable() const { return enable_; }
  std::istream& GetIstream() { return *is_; }
  std::ostream& GetOstream() { return *os_; }
  std::istream* GetIstreamPtr() { return is_; }
  std::ostream* GetOstreamPtr() { return os_; }

 private:
  bool InitIO(char** argv) {
    const auto in_path{std::filesystem::path{argv[1]}};
    if (!in_path.has_filename()) {
      std::cerr << "has not filename: " << in_path << "\n";
      return false;
    }
    std::ifstream* ifs_p{new std::ifstream{in_path}};
    if (!ifs_p->is_open()) {
      std::cerr << "can't open input: " << in_path << "\n";
      delete ifs_p;
      return false;
    }
    const auto out_path{std::filesystem::path{in_path}.replace_extension("gb")};
    std::ofstream* ofs_p{new std::ofstream{out_path, std::ios_base::binary}};
    if (!ofs_p->is_open()) {
      std::cerr << "can't open output: " << out_path << "\n";
      delete ifs_p;
      delete ofs_p;
      return false;
    }
    is_ = ifs_p;
    os_ = ofs_p;
    return true;
  }

 private:
  std::istream* is_;
  std::ostream* os_;
  bool enable_;
};

int main(int argc, char** argv) {
  IOMgr io{argc, argv};
  if (!io.Enable()) {
    std::cerr << "can't open: " << argv[1];
    return -1;
  }

  GBDev::Assembler assembler;
  assembler.Input(io.GetIstream());
  const int err_cnt{assembler.Assemble()};
  if (err_cnt != 0) {
    io.GetIstream().seekg(std::ios_base::beg);
    std::ostringstream oss;
    oss << io.GetIstream().rdbuf();
    const auto errs{assembler.GetErrors()};
    for (const auto& err : errs) {
      GBDev::ErrorMsgHelper::Output(err, oss.str(), &std::cerr);
    }
    return -1;
  }

  auto data{assembler.MoveRomData()};
  io.GetOstream() << data.os.rdbuf();
  assembler.Reset();
}
