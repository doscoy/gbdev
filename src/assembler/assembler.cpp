#include "assembler.hpp"

#include <charconv>
#include <fstream>

#include "myutil.hpp"

namespace GBDev {

namespace {

void OutputToken(const AssemblerImpl::Tokenizer& tokenizer, std::ostream* os) {
  const auto& tokens{tokenizer.GetToken()};
  if (tokens.size() == 0) {
    *os << "no token...\n";
    return;
  }
  *os << "src_id, pos, len, row, col, str, kind\n";
  for (const auto& token : tokens) {
    const std::string str{token.str == "\n" ? "\\n" : token.str};
    *os << token.cursor.src << ", " << token.cursor.pos << ", "
        << token.cursor.len << ", " << token.cursor.row << ", "
        << token.cursor.col << ", \"" << str << "\", " << (int)token.kind
        << "\n";
  }
}

void OutputNode(const AssemblerImpl::Parser& parser, std::ostream* os) {
  const auto& nodes{parser.GetNodes()};
  if (nodes.size() == 0) {
    *os << "no node...\n";
    return;
  }
  *os << "src_id, pos, len, row, col, str, kind\n";
  for (const auto& node : nodes) {
    AssemblerImpl::Parser::PrintMermaid(node, os);
  }
}

void OutputData(const AssemblerImpl::Generator& generator, std::ostream* os) {
  const auto& data{generator.GetData()};
  if (data.defs.size() == 0) {
    *os << "no defs...\n";
  } else {
    *os << "defs\nname, value, kind\n";
    for (const auto& def : data.defs) {
      *os << def.first << ", " << def.second->value << ", "
          << (int)def.second->kind << "\n";
    }
  }
  if (data.refs.size() == 0) {
    *os << "\nno refs...\n";
  } else {
    *os << "\nrefs\nbyte, position, name, kind\n";
    for (const auto& ref : data.refs) {
      AssemblerImpl::Parser::Node::Pointer node;
      int byte;
      int position;
      std::tie(node, byte, position) = ref;
      *os << byte << ", " << position << ", " << node->token.str << ", "
          << (int)node->kind << "\n";
    }
  }
}

void WriteCheckSum(std::vector<char>* raw_bin) {
  constexpr int kHeaderBegAddr{0x0134};
  constexpr int kHeaderEndAddr{0x014C};
  constexpr int kHeaderWriteAddr{0x014D};
  constexpr int kGlobalWriteAddr{0x014E};
  if (raw_bin->size() <= kGlobalWriteAddr + 1) {
    return;
  }
  auto itr{raw_bin->begin()};
  const auto end{raw_bin->end()};
  unsigned int hdr_accum{0};
  unsigned int glb_accum{0};
  for (int i = 0; itr < end; ++itr, ++i) {
    const unsigned char un{static_cast<const unsigned char>(*itr)};
    if (i <= kHeaderEndAddr && i >= kHeaderBegAddr) {
      hdr_accum -= un + 1;
    }
    if (i < kHeaderWriteAddr || i > kGlobalWriteAddr + 1) {
      glb_accum += un;
    }
  }
  glb_accum -=
      (*raw_bin)[kGlobalWriteAddr + 0] + (*raw_bin)[kGlobalWriteAddr + 1];
  const unsigned int hdr_value{hdr_accum & 0xFF};
  const unsigned int glb_value{(glb_accum + hdr_value) & 0xFFFF};
  (*raw_bin)[kHeaderWriteAddr] = static_cast<char>(hdr_value);
  (*raw_bin)[kGlobalWriteAddr + 0] = static_cast<char>(glb_value >> 8);
  (*raw_bin)[kGlobalWriteAddr + 1] = static_cast<char>(glb_value & 0xFF);
}

void RomPadding(std::vector<char>* checksummed_bin) {
  const auto size{checksummed_bin->size()};
  if (size < 0x8000) {
    checksummed_bin->resize(0x8000);
  } else {
    const auto new_size{size % 0x4000 + 0x4000};
    checksummed_bin->resize(new_size);
  }
}

}  // namespace

Assembler::Assembler()
    : tokenizer_(std::make_unique<Tokenizer>()),
      parser_(std::make_unique<Parser>()),
      generator_(std::make_unique<Generator>()) {}

Assembler::~Assembler() {}

int Assembler::Input(const std::string& source) {
  return tokenizer_->SetSource(source);
}

int Assembler::Input(const std::istream& is) {
  using IItr = std::istreambuf_iterator<std::istream::char_type>;
  IItr beg{is.rdbuf()};
  IItr end{};
  std::string tmp{beg, end};
  return Input(tmp);
}

int Assembler::Input(const std::filesystem::path path) {
  std::ifstream ifs{path, std::ios_base::binary};
  if (ifs.is_open()) {
    return Input(ifs);
  } else {
    return -1;
  }
}

int Assembler::Assemble() {
  AssembleImpl();
  return GetErrorCount();
}

int Assembler::Assemble(std::ostream* token_log_os, std::ostream* node_log_os,
                        std::ostream* data_log_os) {
  if (tokenizer_->GetSrcCount() == 0) {
    *token_log_os << "no source input...\n";
    return -1;
  }
  AssembleImpl();
  OutputToken(*tokenizer_, token_log_os);
  OutputNode(*parser_, node_log_os);
  OutputData(*generator_, data_log_os);
  return GetErrorCount();
}

int Assembler::Assemble(std::ostream* log_os) {
  return Assemble(log_os, log_os, log_os);
}

AssemblerImpl::Generator::Data&& Assembler::MoveRawData() {
  return generator_->MoveData();
}

AssemblerImpl::Generator::Data Assembler::MoveRomData() {
  auto tmp_data{MoveRawData()};
  auto tmp_buf{tmp_data.os.movebuf()};
  WriteCheckSum(&tmp_buf);
  RomPadding(&tmp_buf);
  tmp_data.os.movebuf(std::move(tmp_buf));
  return tmp_data;
}

std::vector<char>&& Assembler::GetRawBinary() {
  return generator_->MoveData().os.movebuf();
}

std::vector<char> Assembler::GetRom() {
  std::vector<char> tmp{GetRawBinary()};
  WriteCheckSum(&tmp);
  RomPadding(&tmp);
  return tmp;
}

void Assembler::Reset() {
  tokenizer_.reset(new Tokenizer{});
  parser_.reset(new Parser{});
  generator_.reset(new Generator{});
  ClearErrors();
}

void Assembler::AssembleImpl() {
  parser_ = std::make_unique<Parser>(tokenizer_->GetToken());
  generator_ = std::make_unique<Generator>(parser_->GetNodes());
  MergeErrors(*tokenizer_);
  MergeErrors(*parser_);
  MergeErrors(*generator_);
}

}  // namespace GBDev