#include "generator_impl.hpp"

#include <unordered_set>

#include "myutil.hpp"

namespace GBDev {

namespace AssemblerImpl {

namespace {  // helper functions

namespace ErrMaker {

inline const Parser::Node::Pointer& OPNode(const Parser::Node::Pointer& node) {
  return node->rhs == nullptr ? node : OPNode(node->rhs);
}

inline TextCursor OPCursor(const Parser::Node::Pointer& node) {
  const auto lhs{node->token.cursor};
  const auto rhs{OPNode(node)->token.cursor};
  return {lhs.src, lhs.pos, rhs.pos - lhs.pos + rhs.len, lhs.row, lhs.col};
}

void InvalidInstruct(std::list<ErrorMsg>& err_list,
                     const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: invalid instruct", node->token.cursor);
}

void FailedToResolve(std::list<ErrorMsg>& err_list,
                     const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: failed to resolve reference",
                        node->token.cursor);
}

void InvalidOpcode(std::list<ErrorMsg>& err_list,
                   const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: invalid opcode", node->token.cursor);
}

void NeedOperand(std::list<ErrorMsg>& err_list,
                 const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: need operand", node->token.cursor);
}

void TooManyOperand(std::list<ErrorMsg>& err_list,
                    const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: too many operand", node->token.cursor);
}

void NumOfOperand(std::list<ErrorMsg>& err_list,
                  const Parser::Node::Pointer& node, const int number) {
  err_list.emplace_back("error: number of operand does not match (" +
                            std::to_string(number) + " needed)",
                        node->token.cursor);
}

void InvalidOperand(std::list<ErrorMsg>& err_list,
                    const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: invalid operand", OPCursor(node));
}

void InvalidOperand_ldh(std::list<ErrorMsg>& err_list,
                        const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: only accept \"[a8], a\" or \"a, [a8]\"",
                        node->token.cursor);
}

void InvalidOperand_bit(std::list<ErrorMsg>& err_list,
                        const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: only accept 0 ~ 7", OPCursor(node));
}

void InvalidOperand_rst(std::list<ErrorMsg>& err_list,
                        const Parser::Node::Pointer& node) {
  err_list.emplace_back(
      "error: only accept 00h, 08h, 10h, 18h, 20h, 28h, 30h, 38h",
      OPCursor(node));
}

void InvalidRegister(std::list<ErrorMsg>& err_list,
                     const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: invalid register name", node->token.cursor);
}

void InvalidFlag(std::list<ErrorMsg>& err_list,
                 const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: only accept \"z\", \"c\", \"nz\", \"nc\"",
                        node->token.cursor);
}

void OnlyA(std::list<ErrorMsg>& err_list, const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: only accept \"a\"", node->token.cursor);
}

void OnlyHL(std::list<ErrorMsg>& err_list, const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: only accept \"hl\"", node->token.cursor);
}

void OnlyAorSP(std::list<ErrorMsg>& err_list,
               const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: only accept \"a\" or \"sp\"",
                        node->token.cursor);
}

void LDIorLDD(std::list<ErrorMsg>& err_list,
              const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: only accept \"[hl], a\" or \"a, [hl]\"",
                        node->token.cursor);
}

void PaddingOverflow(std::list<ErrorMsg>& err_list,
                     const Parser::Node::Pointer& node, const int pos) {
  std::ostringstream ss;
  ss << std::hex << std::uppercase << pos << std::dec;
  err_list.emplace_back(
      "error: padding overflow(cursor addres 0x" + ss.str() + ")",
      node->token.cursor);
}

void DuplicateName(std::list<ErrorMsg>& err_list,
                   const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: duplicate name", node->token.cursor);
}

void CircularReference(std::list<ErrorMsg>& err_list,
                       const Parser::Node::Pointer& node) {
  err_list.emplace_back("error: circular reference", node->token.cursor);
}

}  // namespace ErrMaker

namespace OpcodeHelper {

inline int GetOperandCount(const Parser::Node::Pointer& opcode) {
  constexpr auto kFunc{[](const auto& node, const auto func) -> int {
    return node->lhs != nullptr ? 1 + func(node->lhs, func) : 0;
  }};
  return kFunc(opcode, kFunc);
}

}  // namespace OpcodeHelper

namespace OperandHelper {

inline const Parser::Node::Pointer& Type(const Parser::Node::Pointer& operand) {
  return operand->rhs;
}

inline const Parser::Node::Pointer& Value(
    const Parser::Node::Pointer& operand) {
  return operand->rhs->rhs;
}

inline const Parser::Node::Pointer& Formula(
    const Parser::Node::Pointer& operand) {
  return operand->rhs->rhs;
}

inline bool IsImmediate(const Parser::Node::Pointer& operand) {
  return Type(operand)->kind == Parser::Node::Kind::IMMEDIATE;
}

inline bool IsAddress(const Parser::Node::Pointer& operand) {
  return Type(operand)->kind == Parser::Node::Kind::ADDRESS;
}

inline bool IsValue(const Parser::Node::Pointer& operand) {
  return Type(operand)->lhs->kind == Parser::Node::Kind::VALUE;
}

inline bool IsFormula(const Parser::Node::Pointer& operand) {
  return Type(operand)->lhs->kind == Parser::Node::Kind::FORMULA;
}

inline bool IsNumber(const Parser::Node::Pointer& operand) {
  return Value(operand)->kind == Parser::Node::Kind::NUMBER;
}

inline bool IsIdent(const Parser::Node::Pointer& operand) {
  return Value(operand)->kind == Parser::Node::Kind::IDENT;
}

inline bool IsRegister(const Parser::Node::Pointer& operand) {
  return Value(operand)->kind == Parser::Node::Kind::REGISTER;
}

inline bool IsRegister(const Parser::Node::Pointer& operand, const int value) {
  return Value(operand)->kind == Parser::Node::Kind::REGISTER &&
         Value(operand)->value == value;
}

}  // namespace OperandHelper

namespace SymbolHelper {

inline int GetPosition(const Generator::Data& data) {
  using si = std::ios_base;
  return static_cast<int>(data.os.rdbuf()->pubseekoff(0, si::cur, si::out));
}

inline bool FindName(const Generator::Data& data, const std::string& name) {
  return data.defs.find(name) != data.defs.end();
}

inline void StoreDefine(Generator::Data& data, std::string&& name,
                        const int value) {
  using PN = Parser::Node;
  data.defs.emplace(name, PN::MakePtr(PN::Kind::NUMBER, value));
}

inline void StoreDefine(Generator::Data& data, std::string&& name,
                        Parser::Node::Pointer&& node) {
  data.defs.emplace(name, node);
}

inline void StoreReference(Generator::Data& data, Parser::Node::Pointer&& node,
                           const int byte, const int position) {
  data.refs.emplace_back(std::make_tuple(node, byte, position));
}

Parser::Node::Pointer DigIdent(const Parser::Node::Pointer& node) {
  if (node == nullptr) {
    return nullptr;
  } else if (node->kind == Parser::Node::Kind::IDENT) {
    return node;
  }
  const auto& lhs{DigIdent(node->lhs)};
  if (lhs != nullptr && lhs->kind == Parser::Node::Kind::IDENT) {
    return lhs;
  }
  const auto& rhs{DigIdent(node->rhs)};
  if (rhs != nullptr && rhs->kind == Parser::Node::Kind::IDENT) {
    return rhs;
  }
  return nullptr;
}

}  // namespace SymbolHelper

namespace Calculator {

Parser::Node::Pointer Run(Generator::Data& data,
                          std::unordered_set<std::string>& ng,
                          const Parser::Node::Pointer& node);

template <class Lambda>
Parser::Node::Pointer Formula(Generator::Data& data,
                              std::unordered_set<std::string>& ng,
                              const Parser::Node::Pointer& node,
                              const Lambda& impl) {
  using PN = Parser::Node;
  auto lhs{Run(data, ng, node->lhs)};
  if (ng.count("!") != 0) {
    return lhs;
  }
  auto rhs{Run(data, ng, node->rhs)};
  if (ng.count("!") != 0) {
    return rhs;
  } else if (lhs->kind == PN::Kind::NUMBER && rhs->kind == PN::Kind::NUMBER) {
    const auto value{impl(lhs->value, rhs->value)};
    return PN::MakePtr(PN::Kind::NUMBER, value);
  } else {
    auto token{node->token};
    return PN::MakePtr(node->kind, std::move(lhs), std::move(rhs),
                       std::move(token));
  }
}

Parser::Node::Pointer Run(Generator::Data& data,
                          std::unordered_set<std::string>& ng,
                          const Parser::Node::Pointer& node) {
  using PN = Parser::Node;
  switch (node->kind) {
    case PN::Kind::NUMBER:
      return PN::Pointer{node};
    case PN::Kind::IDENT: {
      if (ng.count(node->token.str) != 0) {
        ng.emplace("!");
        return node;
      } else if (SymbolHelper::FindName(data, node->token.str)) {
        const auto value{data.defs[node->token.str]};
        ng.emplace(node->token.str);
        return Run(data, ng, value);
      } else {
        return node;
      }
    }
    case PN::Kind::BITOR:
      return Formula(data, ng, node, [](auto l, auto r) { return l | r; });
    case PN::Kind::BITXOR:
      return Formula(data, ng, node, [](auto l, auto r) { return l ^ r; });
    case PN::Kind::BITAND:
      return Formula(data, ng, node, [](auto l, auto r) { return l & r; });
    case PN::Kind::LSHIFT:
      return Formula(data, ng, node, [](auto l, auto r) { return l << r; });
    case PN::Kind::RSHIFT:
      return Formula(data, ng, node, [](auto l, auto r) { return l >> r; });
    case PN::Kind::ADD:
      return Formula(data, ng, node, [](auto l, auto r) { return l + r; });
    case PN::Kind::SUB:
      return Formula(data, ng, node, [](auto l, auto r) { return l - r; });
    case PN::Kind::MUL:
      return Formula(data, ng, node, [](auto l, auto r) { return l * r; });
    case PN::Kind::DIV:
      return Formula(data, ng, node,
                     [](auto l, auto r) { return r != 0 ? l / r : 0; });
    case PN::Kind::MOD:
      return Formula(data, ng, node, [](auto l, auto r) { return l % r; });
    default:
      return node;
  }
}

}  // namespace Calculator

namespace Write {

inline void Raw(Generator::Data& data, const int value, const int byte) {
  data.os << Util::BinOut(value, byte);
}

inline void Raw(Generator::Data& data, const int value, const int byte,
                const int position) {
  const auto backup{data.os.tellp()};
  data.os.seekp(position);
  Raw(data, value, byte);
  data.os.seekp(backup);
}

inline void Reference(Generator::Data& data, const Parser::Node::Pointer& node,
                      const int byte) {
  if (SymbolHelper::FindName(data, node->token.str)) {
    const auto value{data.defs[node->token.str]};
    if (value->kind == Parser::Node::Kind::NUMBER) {
      return Raw(data, value->value, byte);
    }
  }
  auto copy_node{node};
  const auto position{SymbolHelper::GetPosition(data)};
  SymbolHelper::StoreReference(data, std::move(copy_node), byte, position);
  return Raw(data, Util::FillCharInt<int>('?'), byte);
}

inline void Formula(Generator::Data& data, const Parser::Node::Pointer& node,
                    const int byte) {
  std::unordered_set<std::string> ng;
  auto ans{Calculator::Run(data, ng, node)};
  if (ng.count("!") != 0) {
    return Reference(data, node, byte);
  } else if (ans->kind == Parser::Node::Kind::NUMBER) {
    return Raw(data, ans->value, byte);
  } else {
    return Reference(data, ans, byte);
  }
}

inline void Operand(Generator::Data& data, std::list<ErrorMsg>& err_list,
                    const Parser::Node::Pointer& operand, const int byte) {
  if (OperandHelper::IsFormula(operand)) {
    return Formula(data, OperandHelper::Formula(operand), byte);
  } else if (OperandHelper::IsValue(operand)) {
    if (OperandHelper::IsNumber(operand)) {
      return Raw(data, OperandHelper::Value(operand)->value, byte);
    } else if (OperandHelper::IsIdent(operand)) {
      return Reference(data, OperandHelper::Value(operand), byte);
    }
  }
  Raw(data, Util::FillCharInt<int>('!'), byte);
  return ErrMaker::InvalidOperand(err_list, operand);
}

inline void MachineCode(Generator::Data& data, const int code) {
  Raw(data, code, 1);
}

inline void MachineCode(Generator::Data& data, const int code,
                        const int additional) {
  Raw(data, code, 1);
  Raw(data, additional, 1);
}

inline void MachineCode(Generator::Data& data, const int code,
                        std::list<ErrorMsg>& err_list,
                        const Parser::Node::Pointer& operand, const int byte) {
  if (!OperandHelper::IsRegister(operand)) {
    Raw(data, code, 1);
    Operand(data, err_list, operand, byte);
  } else {
    ErrMaker::InvalidOperand(err_list, operand);
  }
}

}  // namespace Write

}  // namespace

void GeneratorImpl::Generate(const Parser::NodeContainer& nodes) {
  for (const auto& node : nodes) {
    if (node == nullptr) {
      continue;
    }
    switch (node->kind) {
      case Node::Kind::LABEL: {
        std::string name{node->token.str};
        if (!SymbolHelper::FindName(base_.data_, name)) {
          const auto pos{SymbolHelper::GetPosition(base_.data_)};
          SymbolHelper::StoreDefine(base_.data_, std::move(name), pos);
        } else {
          ErrMaker::DuplicateName(base_.errors_, node);
        }
        if (node->lhs != nullptr && node->lhs->kind == Node::Kind::OPCODE) {
          Opcode(node->lhs);
        }
      } break;
      case Node::Kind::OPCODE:
        Opcode(node);
        break;
      default:
        ErrMaker::InvalidInstruct(base_.errors_, node);
        break;
    }
  }
  ResolvingReference();
  if (base_.data_.refs.size() != 0) {
    for (const auto& ref : base_.data_.refs) {
      const auto node{SymbolHelper::DigIdent(std::get<0>(ref))};
      if (node != nullptr) {
        ErrMaker::FailedToResolve(base_.errors_, node);
      } else {
        ErrMaker::FailedToResolve(base_.errors_, std::get<0>(ref));
      }
    }
  }
}

void GeneratorImpl::ResolvingReference() {
  decltype(base_.data_.refs) buf;
  for (const auto& ref : base_.data_.refs) {
    Parser::Node::Pointer node;
    int byte;
    int position;
    std::tie(node, byte, position) = ref;
    std::unordered_set<std::string> ng;
    node = Calculator::Run(base_.data_, ng, node);
    if (ng.count("!") != 0) {
      ErrMaker::CircularReference(base_.errors_, node);
    } else if (node->kind == Parser::Node::Kind::NUMBER) {
      Write::Raw(base_.data_, node->value, byte, position);
    } else {
      buf.emplace_back(node, byte, position);
    }
  }
  base_.data_.refs = buf;
}

void GeneratorImpl::Opcode(const Node::Pointer& opcode) {
  switch (opcode->value) {
    case Util::EncodeBase36<int>("ld"):
      return OP_ld(opcode);
    case Util::EncodeBase36<int>("ldi"):
      return OP_ldi(opcode);
    case Util::EncodeBase36<int>("ldd"):
      return OP_ldd(opcode);
    case Util::EncodeBase36<int>("ldh"):
      return OP_ldh(opcode);
    case Util::EncodeBase36<int>("push"):
      return OP_push_Or_pop(opcode, 0xC5);
    case Util::EncodeBase36<int>("pop"):
      return OP_push_Or_pop(opcode, 0xC1);
    case Util::EncodeBase36<int>("add"):
      return OP_add(opcode);
    case Util::EncodeBase36<int>("adc"):
      return OP_xx_ImmRegA_XX(opcode, 0x88, 0xCE);
    case Util::EncodeBase36<int>("sub"):
      return OP_xx_ImmRegA_XX(opcode, 0x90, 0xD6);
    case Util::EncodeBase36<int>("sbc"):
      return OP_xx_ImmRegA_XX(opcode, 0x98, 0xDE);
    case Util::EncodeBase36<int>("and"):
      return OP_xx_ImmRegA_XX(opcode, 0xA0, 0xE6);
    case Util::EncodeBase36<int>("xor"):
      return OP_xx_ImmRegA_XX(opcode, 0xA8, 0xEE);
    case Util::EncodeBase36<int>("or"):
      return OP_xx_ImmRegA_XX(opcode, 0xB0, 0xF6);
    case Util::EncodeBase36<int>("cp"):
      return OP_xx_ImmRegA_XX(opcode, 0xB8, 0xFE);
    case Util::EncodeBase36<int>("inc"):
      return OP_inc(opcode);
    case Util::EncodeBase36<int>("dec"):
      return OP_dec(opcode);
    case Util::EncodeBase36<int>("daa"):
      return OP_xx(opcode, 0x27);
    case Util::EncodeBase36<int>("cpl"):
      return OP_xx(opcode, 0x2F);
    case Util::EncodeBase36<int>("rlca"):
      return OP_xx(opcode, 0x07);
    case Util::EncodeBase36<int>("rla"):
      return OP_xx(opcode, 0x17);
    case Util::EncodeBase36<int>("rrca"):
      return OP_xx(opcode, 0x0F);
    case Util::EncodeBase36<int>("rra"):
      return OP_xx(opcode, 0x1F);
    case Util::EncodeBase36<int>("rlc"):
      return OP_cb(opcode, 0x00);
    case Util::EncodeBase36<int>("rl"):
      return OP_cb(opcode, 0x10);
    case Util::EncodeBase36<int>("rrc"):
      return OP_cb(opcode, 0x08);
    case Util::EncodeBase36<int>("rr"):
      return OP_cb(opcode, 0x18);
    case Util::EncodeBase36<int>("sla"):
      return OP_cb(opcode, 0x20);
    case Util::EncodeBase36<int>("swap"):
      return OP_cb(opcode, 0x30);
    case Util::EncodeBase36<int>("sra"):
      return OP_cb(opcode, 0x28);
    case Util::EncodeBase36<int>("srl"):
      return OP_cb(opcode, 0x38);
    case Util::EncodeBase36<int>("bit"):
      return OP_cb_bit(opcode, 0x40);
    case Util::EncodeBase36<int>("set"):
      return OP_cb_bit(opcode, 0xC0);
    case Util::EncodeBase36<int>("res"):
      return OP_cb_bit(opcode, 0x80);
    case Util::EncodeBase36<int>("ccf"):
      return OP_xx(opcode, 0x3F);
    case Util::EncodeBase36<int>("scf"):
      return OP_xx(opcode, 0x37);
    case Util::EncodeBase36<int>("nop"):
      return OP_xx(opcode, 0x00);
    case Util::EncodeBase36<int>("halt"):
      return OP_xx(opcode, 0x76);
    case Util::EncodeBase36<int>("stop"):
      return OP_stop(opcode);
    case Util::EncodeBase36<int>("di"):
      return OP_xx(opcode, 0xF3);
    case Util::EncodeBase36<int>("ei"):
      return OP_xx(opcode, 0xFB);
    case Util::EncodeBase36<int>("jp"):
      return OP_jp(opcode);
    case Util::EncodeBase36<int>("jr"):
      return OP_jr(opcode);
    case Util::EncodeBase36<int>("call"):
      return OP_call(opcode);
    case Util::EncodeBase36<int>("ret"):
      return OP_ret(opcode);
    case Util::EncodeBase36<int>("reti"):
      return OP_xx(opcode, 0xD9);
    case Util::EncodeBase36<int>("rst"):
      return OP_rst(opcode);
    case Util::EncodeBase36<int>("db"):
      return OP_db(opcode);
    case Util::EncodeBase36<int>("ds"):
      return OP_ds(opcode);
    case Util::EncodeBase36<int>("dp"):
      return OP_dp(opcode);
    case Util::EncodeBase36<int>("def"):
      return OP_def(opcode);
    default:
      return ErrMaker::InvalidOpcode(base_.errors_, opcode);
  }
}

void GeneratorImpl::OP_xx(const Node::Pointer& opcode, const int num) {
  constexpr int kOperandCount{0};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  } else {
    return Write::MachineCode(base_.data_, num);
  }
}

void GeneratorImpl::OP_xx_ImmRegX8(const Node::Pointer& operand_2,
                                   const int reg, const int num) {
  switch (OperandHelper::Type(operand_2)->kind) {
    case Node::Kind::IMMEDIATE:
      return OP_xx_ImmRegX8_Imm(operand_2, reg, num);
    case Node::Kind::ADDRESS:
      return OP_xx_ImmRegX8_Addr(operand_2, reg + 6);
    default:
      return ErrMaker::InvalidOperand(base_.errors_, operand_2);
  }
}

void GeneratorImpl::OP_xx_ImmRegX8_Imm(const Node::Pointer& operand_2,
                                       const int reg, const int num) {
  if (OperandHelper::IsRegister(operand_2)) {
    return OP_xx_ImmRegX8_ImmReg(operand_2, reg);
  } else {
    return Write::MachineCode(base_.data_, num, base_.errors_, operand_2, 1);
  }
}

void GeneratorImpl::OP_xx_ImmRegX8_ImmReg(const Node::Pointer& operand_2,
                                          const int reg) {
  switch (OperandHelper::Value(operand_2)->value) {
    case Util::EncodeBase36<int>("b"):
      return Write::MachineCode(base_.data_, reg + 0);
    case Util::EncodeBase36<int>("c"):
      return Write::MachineCode(base_.data_, reg + 1);
    case Util::EncodeBase36<int>("d"):
      return Write::MachineCode(base_.data_, reg + 2);
    case Util::EncodeBase36<int>("e"):
      return Write::MachineCode(base_.data_, reg + 3);
    case Util::EncodeBase36<int>("h"):
      return Write::MachineCode(base_.data_, reg + 4);
    case Util::EncodeBase36<int>("l"):
      return Write::MachineCode(base_.data_, reg + 5);
    case Util::EncodeBase36<int>("a"):
      return Write::MachineCode(base_.data_, reg + 7);
    default:
      return ErrMaker::InvalidRegister(base_.errors_, operand_2);
  }
}

void GeneratorImpl::OP_xx_ImmRegX8_Addr(const Node::Pointer& operand_2,
                                        const int reg) {
  if (OperandHelper::IsRegister(operand_2, Util::EncodeBase36<int>("hl"))) {
    return Write::MachineCode(base_.data_, reg);
  }
  return ErrMaker::OnlyHL(base_.errors_, operand_2);
}

void GeneratorImpl::OP_xx_ImmRegA_XX(const Node::Pointer& opcode, const int reg,
                                     const int num) {
  const auto operand_count{OpcodeHelper::GetOperandCount(opcode)};
  if (operand_count == 0) {
    return ErrMaker::NeedOperand(base_.errors_, opcode);
  } else if (operand_count == 1) {
    const auto operand{opcode->lhs};
    return OP_xx_ImmRegX8(operand, reg, num);
  } else if (operand_count == 2) {
    const auto operand_1{opcode->lhs};
    const auto operand_2{opcode->lhs->lhs};
    if (!OperandHelper::IsImmediate(operand_1) ||
        !OperandHelper::IsRegister(operand_1)) {
      return ErrMaker::InvalidOperand(base_.errors_, operand_1);
    }
    if (OperandHelper::Value(operand_1)->value ==
        Util::EncodeBase36<int>("a")) {
      return OP_xx_ImmRegX8(operand_2, reg, num);
    } else {
      return ErrMaker::OnlyA(base_.errors_, operand_1);
    }
  } else {
    return ErrMaker::TooManyOperand(base_.errors_, opcode);
  }
}

void GeneratorImpl::OP_ld(const Node::Pointer& opcode) {
  constexpr int kOperandCount{2};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  }
  const auto operand_1{opcode->lhs};
  const auto operand_2{opcode->lhs->lhs};
  switch (OperandHelper::Type(operand_1)->kind) {
    case Node::Kind::IMMEDIATE:
      return OP_ld_Imm(operand_1, operand_2);
    case Node::Kind::ADDRESS:
      return OP_ld_Addr(operand_1, operand_2);
    default:
      return ErrMaker::InvalidOperand(base_.errors_, operand_1);
  }
}

void GeneratorImpl::OP_ld_Imm(const Node::Pointer& operand_1,
                              const Node::Pointer& operand_2) {
  if (OperandHelper::IsFormula(operand_1) ||
      OperandHelper::Value(operand_1)->kind != Node::Kind::REGISTER) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_1);
  }
  switch (OperandHelper::Value(operand_1)->value) {
    case Util::EncodeBase36<int>("a"):
      return OP_ld_ImmRegA(operand_2);
    case Util::EncodeBase36<int>("b"):
      return OP_xx_ImmRegX8(operand_2, 0x40, 0x06);
    case Util::EncodeBase36<int>("c"):
      return OP_xx_ImmRegX8(operand_2, 0x48, 0x0E);
    case Util::EncodeBase36<int>("d"):
      return OP_xx_ImmRegX8(operand_2, 0x50, 0x16);
    case Util::EncodeBase36<int>("e"):
      return OP_xx_ImmRegX8(operand_2, 0x58, 0x1E);
    case Util::EncodeBase36<int>("h"):
      return OP_xx_ImmRegX8(operand_2, 0x60, 0x26);
    case Util::EncodeBase36<int>("l"):
      return OP_xx_ImmRegX8(operand_2, 0x68, 0x2E);
    case Util::EncodeBase36<int>("bc"):
      return OP_ld_ImmRegX16(operand_2, 0x01);
    case Util::EncodeBase36<int>("de"):
      return OP_ld_ImmRegX16(operand_2, 0x11);
    case Util::EncodeBase36<int>("hl"):
      return OP_ld_ImmRegX16(operand_2, 0x21);
    case Util::EncodeBase36<int>("sp"):
      return OP_ld_ImmRegSP(operand_2);
    default:
      return ErrMaker::InvalidRegister(base_.errors_, operand_1);
  }
}

void GeneratorImpl::OP_ld_ImmRegA(const Node::Pointer& operand_2) {
  switch (OperandHelper::Type(operand_2)->kind) {
    case Node::Kind::IMMEDIATE:
      return OP_xx_ImmRegX8_Imm(operand_2, 0x78, 0x3E);
    case Node::Kind::ADDRESS:
      return OP_ld_ImmRegA_Addr(operand_2);
    default:
      return ErrMaker::InvalidOperand(base_.errors_, operand_2);
  }
}

void GeneratorImpl::OP_ld_ImmRegA_Addr(const Node::Pointer& operand_2) {
  if (OperandHelper::IsRegister(operand_2)) {
    return OP_ld_ImmRegA_AddrReg(operand_2);
  } else {
    return Write::MachineCode(base_.data_, 0xFA, base_.errors_, operand_2, 2);
  }
}

void GeneratorImpl::OP_ld_ImmRegA_AddrReg(const Node::Pointer& operand_2) {
  switch (OperandHelper::Value(operand_2)->value) {
    case Util::EncodeBase36<int>("c"):
      return Write::MachineCode(base_.data_, 0xF2);
    case Util::EncodeBase36<int>("bc"):
      return Write::MachineCode(base_.data_, 0x0A);
    case Util::EncodeBase36<int>("de"):
      return Write::MachineCode(base_.data_, 0x1A);
    case Util::EncodeBase36<int>("hl"):
      return Write::MachineCode(base_.data_, 0x7E);
    default:
      return ErrMaker::InvalidRegister(base_.errors_, operand_2);
  }
}

void GeneratorImpl::OP_ld_ImmRegX16(const Node::Pointer& operand_2,
                                    const int num) {
  if (!OperandHelper::IsImmediate(operand_2)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_2);
  } else {
    return Write::MachineCode(base_.data_, num, base_.errors_, operand_2, 2);
  }
}

void GeneratorImpl::OP_ld_ImmRegSP(const Node::Pointer& operand_2) {
  if (!OperandHelper::IsImmediate(operand_2)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_2);
  }
  if (OperandHelper::IsRegister(operand_2)) {
    if (OperandHelper::Value(operand_2)->value ==
        Util::EncodeBase36<int>("hl")) {
      return Write::MachineCode(base_.data_, 0xF9);
    } else {
      return ErrMaker::OnlyHL(base_.errors_, operand_2);
    }
  } else {
    return OP_ld_ImmRegX16(operand_2, 0x31);
  }
}

void GeneratorImpl::OP_ld_Addr(const Node::Pointer& operand_1,
                               const Node::Pointer& operand_2) {
  if (!OperandHelper::IsImmediate(operand_2)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_2);
  }
  if (OperandHelper::IsRegister(operand_1)) {
    return OP_ld_AddrReg(operand_1, operand_2);
  }
  if (!OperandHelper::IsRegister(operand_2)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_2);
  }
  switch (OperandHelper::Value(operand_2)->value) {
    case Util::EncodeBase36<int>("sp"):
      return Write::MachineCode(base_.data_, 0x08, base_.errors_, operand_1, 2);
    case Util::EncodeBase36<int>("a"):
      return Write::MachineCode(base_.data_, 0xEA, base_.errors_, operand_1, 2);
    default:
      return ErrMaker::OnlyAorSP(base_.errors_, operand_2);
  }
}

void GeneratorImpl::OP_ld_AddrReg(const Node::Pointer& operand_1,
                                  const Node::Pointer& operand_2) {
  switch (OperandHelper::Value(operand_1)->value) {
    case Util::EncodeBase36<int>("c"):
      return OP_ld_AddrRegX(operand_2, 0xE2);
    case Util::EncodeBase36<int>("bc"):
      return OP_ld_AddrRegX(operand_2, 0x02);
    case Util::EncodeBase36<int>("de"):
      return OP_ld_AddrRegX(operand_2, 0x12);
    case Util::EncodeBase36<int>("hl"):
      return OP_ld_AddrRegHL(operand_2);
    default:
      return ErrMaker::InvalidRegister(base_.errors_, operand_1);
  }
}

void GeneratorImpl::OP_ld_AddrRegX(const Node::Pointer& operand_2,
                                   const int num) {
  if (OperandHelper::IsImmediate(operand_2) &&
      OperandHelper::IsRegister(operand_2, Util::EncodeBase36<int>("a"))) {
    return Write::MachineCode(base_.data_, num);
  }
  return ErrMaker::OnlyHL(base_.errors_, operand_2);
}

void GeneratorImpl::OP_ld_AddrRegHL(const Node::Pointer& operand_2) {
  if (!OperandHelper::IsImmediate(operand_2)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_2);
  }
  if (OperandHelper::IsRegister(operand_2)) {
    return OP_ld_AddrRegHL_ImmReg(operand_2);
  }
  return Write::MachineCode(base_.data_, 0x36, base_.errors_, operand_2, 1);
}

void GeneratorImpl::OP_ld_AddrRegHL_ImmReg(const Node::Pointer& operand_2) {
  switch (OperandHelper::Value(operand_2)->value) {
    case Util::EncodeBase36<int>("b"):
      return Write::MachineCode(base_.data_, 0x70);
    case Util::EncodeBase36<int>("c"):
      return Write::MachineCode(base_.data_, 0x71);
    case Util::EncodeBase36<int>("d"):
      return Write::MachineCode(base_.data_, 0x72);
    case Util::EncodeBase36<int>("e"):
      return Write::MachineCode(base_.data_, 0x73);
    case Util::EncodeBase36<int>("h"):
      return Write::MachineCode(base_.data_, 0x74);
    case Util::EncodeBase36<int>("l"):
      return Write::MachineCode(base_.data_, 0x75);
    case Util::EncodeBase36<int>("a"):
      return Write::MachineCode(base_.data_, 0x77);
    default:
      return ErrMaker::InvalidRegister(base_.errors_, operand_2);
  }
}

void GeneratorImpl::OP_ldi(const Node::Pointer& opcode) {
  constexpr int kOperandCount{2};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  }
  const auto operand_1{opcode->lhs};
  const auto operand_2{opcode->lhs->lhs};
  if (OperandHelper::IsAddress(operand_1) &&
      OperandHelper::IsRegister(operand_1, Util::EncodeBase36<int>("hl")) &&
      OperandHelper::IsImmediate(operand_2) &&
      OperandHelper::IsRegister(operand_2, Util::EncodeBase36<int>("a"))) {
    return Write::MachineCode(base_.data_, 0x22);
  }
  if (OperandHelper::IsImmediate(operand_1) &&
      OperandHelper::IsRegister(operand_1, Util::EncodeBase36<int>("a")) &&
      OperandHelper::IsAddress(operand_2) &&
      OperandHelper::IsRegister(operand_2, Util::EncodeBase36<int>("hl"))) {
    return Write::MachineCode(base_.data_, 0x2A);
  }
  return ErrMaker::LDIorLDD(base_.errors_, opcode);
}

void GeneratorImpl::OP_ldd(const Node::Pointer& opcode) {
  constexpr int kOperandCount{2};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  }
  const auto operand_1{opcode->lhs};
  const auto operand_2{opcode->lhs->lhs};
  if (OperandHelper::IsAddress(operand_1) &&
      OperandHelper::IsRegister(operand_1, Util::EncodeBase36<int>("hl")) &&
      OperandHelper::IsImmediate(operand_2) &&
      OperandHelper::IsRegister(operand_2, Util::EncodeBase36<int>("a"))) {
    return Write::MachineCode(base_.data_, 0x32);
  }
  if (OperandHelper::IsImmediate(operand_1) &&
      OperandHelper::IsRegister(operand_1, Util::EncodeBase36<int>("a")) &&
      OperandHelper::IsAddress(operand_2) &&
      OperandHelper::IsRegister(operand_2, Util::EncodeBase36<int>("hl"))) {
    return Write::MachineCode(base_.data_, 0x3A);
  }
  return ErrMaker::LDIorLDD(base_.errors_, opcode);
}

void GeneratorImpl::OP_ldh(const Node::Pointer& opcode) {
  constexpr int kOperandCount{2};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  }
  const auto operand_1{opcode->lhs};
  const auto operand_2{opcode->lhs->lhs};
  if (OperandHelper::IsAddress(operand_1) &&
      OperandHelper::IsImmediate(operand_2) &&
      OperandHelper::IsRegister(operand_2, Util::EncodeBase36<int>("a"))) {
    return Write::MachineCode(base_.data_, 0xE0, base_.errors_, operand_1, 1);
  }
  if (OperandHelper::IsImmediate(operand_1) &&
      OperandHelper::IsRegister(operand_1, Util::EncodeBase36<int>("a")) &&
      OperandHelper::IsAddress(operand_2)) {
    return Write::MachineCode(base_.data_, 0xF0, base_.errors_, operand_2, 1);
  }
  return ErrMaker::InvalidOperand_ldh(base_.errors_, opcode);
}

void GeneratorImpl::OP_push_Or_pop(const Node::Pointer& opcode,
                                   const int base) {
  constexpr int kOperandCount{1};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  }
  const auto operand{opcode->lhs};
  if (OperandHelper::IsImmediate(operand) &&
      OperandHelper::IsRegister(operand)) {
    switch (OperandHelper::Value(operand)->value) {
      case Util::EncodeBase36<int>("bc"):
        return Write::MachineCode(base_.data_, base + 0x00);
      case Util::EncodeBase36<int>("de"):
        return Write::MachineCode(base_.data_, base + 0x10);
      case Util::EncodeBase36<int>("hl"):
        return Write::MachineCode(base_.data_, base + 0x20);
      case Util::EncodeBase36<int>("af"):
        return Write::MachineCode(base_.data_, base + 0x30);
      default:
        return ErrMaker::InvalidRegister(base_.errors_, operand);
    }
  } else {
    return ErrMaker::InvalidOperand(base_.errors_, operand);
  }
}

void GeneratorImpl::OP_add(const Node::Pointer& opcode) {
  const auto operand_count{OpcodeHelper::GetOperandCount(opcode)};
  if (operand_count == 0) {
    return ErrMaker::NeedOperand(base_.errors_, opcode);
  } else if (operand_count == 1) {
    const auto operand{opcode->lhs};
    return OP_xx_ImmRegX8(operand, 0x80, 0xC6);
  } else if (operand_count == 2) {
    const auto operand_1{opcode->lhs};
    const auto operand_2{opcode->lhs->lhs};
    if (!OperandHelper::IsImmediate(operand_1) ||
        !OperandHelper::IsRegister(operand_1)) {
      return ErrMaker::InvalidOperand(base_.errors_, operand_1);
    }
    switch (OperandHelper::Value(operand_1)->value) {
      case Util::EncodeBase36<int>("a"):
        return OP_xx_ImmRegX8(operand_2, 0x80, 0xC6);
      case Util::EncodeBase36<int>("hl"):
        return OP_add_hl(operand_2);
      case Util::EncodeBase36<int>("sp"):
        return Write::MachineCode(base_.data_, 0xE8, base_.errors_, operand_2,
                                  1);
      default:
        return ErrMaker::InvalidRegister(base_.errors_, operand_1);
    }
  } else {
    return ErrMaker::TooManyOperand(base_.errors_, opcode);
  }
}

void GeneratorImpl::OP_add_hl(const Node::Pointer& operand_2) {
  if (!OperandHelper::IsImmediate(operand_2) ||
      !OperandHelper::IsRegister(operand_2)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_2);
  }
  switch (OperandHelper::Value(operand_2)->value) {
    case Util::EncodeBase36<int>("bc"):
      return Write::MachineCode(base_.data_, 0x09);
    case Util::EncodeBase36<int>("de"):
      return Write::MachineCode(base_.data_, 0x19);
    case Util::EncodeBase36<int>("hl"):
      return Write::MachineCode(base_.data_, 0x29);
    case Util::EncodeBase36<int>("sp"):
      return Write::MachineCode(base_.data_, 0x39);
    default:
      return ErrMaker::InvalidRegister(base_.errors_, operand_2);
  }
}

void GeneratorImpl::OP_inc(const Node::Pointer& opcode) {
  constexpr int kOperandCount{1};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  }
  const auto operand{opcode->lhs};
  switch (OperandHelper::Type(operand)->kind) {
    case Node::Kind::IMMEDIATE:
      return OP_inc_Imm(operand);
    case Node::Kind::ADDRESS:
      return OP_inc_Addr(operand);
    default:
      return ErrMaker::InvalidOperand(base_.errors_, operand);
  }
}

void GeneratorImpl::OP_inc_Imm(const Node::Pointer& operand) {
  if (!OperandHelper::IsRegister(operand)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand);
  }
  switch (OperandHelper::Value(operand)->value) {
    case Util::EncodeBase36<int>("b"):
      return Write::MachineCode(base_.data_, 0x04);
    case Util::EncodeBase36<int>("c"):
      return Write::MachineCode(base_.data_, 0x0C);
    case Util::EncodeBase36<int>("d"):
      return Write::MachineCode(base_.data_, 0x14);
    case Util::EncodeBase36<int>("e"):
      return Write::MachineCode(base_.data_, 0x1C);
    case Util::EncodeBase36<int>("h"):
      return Write::MachineCode(base_.data_, 0x24);
    case Util::EncodeBase36<int>("l"):
      return Write::MachineCode(base_.data_, 0x2C);
    case Util::EncodeBase36<int>("bc"):
      return Write::MachineCode(base_.data_, 0x03);
    case Util::EncodeBase36<int>("de"):
      return Write::MachineCode(base_.data_, 0x13);
    case Util::EncodeBase36<int>("hl"):
      return Write::MachineCode(base_.data_, 0x23);
    case Util::EncodeBase36<int>("sp"):
      return Write::MachineCode(base_.data_, 0x33);
    case Util::EncodeBase36<int>("a"):
      return Write::MachineCode(base_.data_, 0x3C);
    default:
      return ErrMaker::InvalidRegister(base_.errors_, operand);
  }
}

void GeneratorImpl::OP_inc_Addr(const Node::Pointer& operand) {
  if (OperandHelper::IsRegister(operand, Util::EncodeBase36<int>("hl"))) {
    return Write::MachineCode(base_.data_, 0x34);
  } else {
    return ErrMaker::OnlyHL(base_.errors_, operand);
  }
}

void GeneratorImpl::OP_dec(const Node::Pointer& opcode) {
  constexpr int kOperandCount{1};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  }
  const auto operand{opcode->lhs};
  switch (OperandHelper::Type(operand)->kind) {
    case Node::Kind::IMMEDIATE:
      return OP_dec_Imm(operand);
    case Node::Kind::ADDRESS:
      return OP_dec_Addr(operand);
    default:
      return ErrMaker::InvalidOperand(base_.errors_, operand);
  }
}

void GeneratorImpl::OP_dec_Imm(const Node::Pointer& operand) {
  if (!OperandHelper::IsRegister(operand)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand);
  }
  switch (OperandHelper::Value(operand)->value) {
    case Util::EncodeBase36<int>("b"):
      return Write::MachineCode(base_.data_, 0x05);
    case Util::EncodeBase36<int>("c"):
      return Write::MachineCode(base_.data_, 0x0D);
    case Util::EncodeBase36<int>("d"):
      return Write::MachineCode(base_.data_, 0x15);
    case Util::EncodeBase36<int>("e"):
      return Write::MachineCode(base_.data_, 0x1D);
    case Util::EncodeBase36<int>("h"):
      return Write::MachineCode(base_.data_, 0x25);
    case Util::EncodeBase36<int>("l"):
      return Write::MachineCode(base_.data_, 0x2D);
    case Util::EncodeBase36<int>("bc"):
      return Write::MachineCode(base_.data_, 0x0B);
    case Util::EncodeBase36<int>("de"):
      return Write::MachineCode(base_.data_, 0x1B);
    case Util::EncodeBase36<int>("hl"):
      return Write::MachineCode(base_.data_, 0x2B);
    case Util::EncodeBase36<int>("sp"):
      return Write::MachineCode(base_.data_, 0x3B);
    case Util::EncodeBase36<int>("a"):
      return Write::MachineCode(base_.data_, 0x3D);
    default:
      return ErrMaker::InvalidRegister(base_.errors_, operand);
  }
}

void GeneratorImpl::OP_dec_Addr(const Node::Pointer& operand) {
  if (OperandHelper::IsRegister(operand, Util::EncodeBase36<int>("hl"))) {
    return Write::MachineCode(base_.data_, 0x35);
  } else {
    return ErrMaker::OnlyHL(base_.errors_, operand);
  }
}

void GeneratorImpl::OP_cb(const Node::Pointer& opcode, const int base) {
  constexpr int kOperandCount{1};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  }
  const auto operand{opcode->lhs};
  switch (OperandHelper::Type(operand)->kind) {
    case Node::Kind::IMMEDIATE:
      return OP_cb_Imm(operand, base);
    case Node::Kind::ADDRESS:
      return OP_cb_Addr(operand, base);
    default:
      return ErrMaker::InvalidOperand(base_.errors_, operand);
  }
}

void GeneratorImpl::OP_cb_Imm(const Node::Pointer& operand, const int base) {
  if (!OperandHelper::IsRegister(operand)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand);
  }
  switch (OperandHelper::Value(operand)->value) {
    case Util::EncodeBase36<int>("b"):
      return Write::MachineCode(base_.data_, 0xCB, base + 0);
    case Util::EncodeBase36<int>("c"):
      return Write::MachineCode(base_.data_, 0xCB, base + 1);
    case Util::EncodeBase36<int>("d"):
      return Write::MachineCode(base_.data_, 0xCB, base + 2);
    case Util::EncodeBase36<int>("e"):
      return Write::MachineCode(base_.data_, 0xCB, base + 3);
    case Util::EncodeBase36<int>("h"):
      return Write::MachineCode(base_.data_, 0xCB, base + 4);
    case Util::EncodeBase36<int>("l"):
      return Write::MachineCode(base_.data_, 0xCB, base + 5);
    case Util::EncodeBase36<int>("a"):
      return Write::MachineCode(base_.data_, 0xCB, base + 7);
    default:
      return ErrMaker::InvalidRegister(base_.errors_, operand);
  }
}

void GeneratorImpl::OP_cb_Addr(const Node::Pointer& operand, const int base) {
  if (OperandHelper::IsRegister(operand, Util::EncodeBase36<int>("hl"))) {
    return Write::MachineCode(base_.data_, 0xCB, base + 6);
  } else {
    return ErrMaker::OnlyHL(base_.errors_, operand);
  }
}

void GeneratorImpl::OP_cb_bit(const Node::Pointer& opcode, const int base) {
  constexpr int kOperandCount{2};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  }
  const auto operand_1{opcode->lhs};
  const auto operand_2{opcode->lhs->lhs};
  if (!OperandHelper::IsImmediate(operand_1) ||
      !OperandHelper::IsNumber(operand_1) ||
      OperandHelper::Value(operand_1)->value < 0 ||
      OperandHelper::Value(operand_1)->value > 7) {
    return ErrMaker::InvalidOperand_bit(base_.errors_, operand_1);
  } else if (OperandHelper::IsImmediate(operand_2)) {
    const auto op1_v{OperandHelper::Value(operand_1)->value};
    return OP_cb_Imm(operand_2, base + 8 * op1_v);
  } else if (OperandHelper::IsAddress(operand_2)) {
    const auto op1_v{OperandHelper::Value(operand_1)->value};
    return OP_cb_Addr(operand_2, base + 8 * op1_v);
  } else {
    return ErrMaker::InvalidOperand(base_.errors_, operand_2);
  }
}

void GeneratorImpl::OP_stop(const Node::Pointer& opcode) {
  constexpr int kOperandCount{0};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  } else {
    return Write::MachineCode(base_.data_, 0x10, 0x00);
  }
}

void GeneratorImpl::OP_jp(const Node::Pointer& opcode) {
  const auto operand_count{OpcodeHelper::GetOperandCount(opcode)};
  if (operand_count == 0) {
    return ErrMaker::NeedOperand(base_.errors_, opcode);
  } else if (operand_count == 1) {
    return OP_jp_xx(opcode->lhs);
  } else if (operand_count == 2) {
    return OP_jp_xx_xx(opcode->lhs, opcode->lhs->lhs);
  } else {
    return ErrMaker::TooManyOperand(base_.errors_, opcode);
  }
}

void GeneratorImpl::OP_jp_xx(const Node::Pointer& operand) {
  if (!OperandHelper::IsImmediate(operand)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand);
  }
  if (OperandHelper::IsRegister(operand)) {
    if (OperandHelper::Value(operand)->value == Util::EncodeBase36<int>("hl")) {
      return Write::MachineCode(base_.data_, 0xE9);
    } else {
      return ErrMaker::OnlyHL(base_.errors_, operand);
    }
  }
  return Write::MachineCode(base_.data_, 0xC3, base_.errors_, operand, 2);
}

void GeneratorImpl::OP_jp_xx_xx(const Node::Pointer& operand_1,
                                const Node::Pointer& operand_2) {
  if (!OperandHelper::IsImmediate(operand_1) ||
      !OperandHelper::IsRegister(operand_1)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_1);
  } else if (!OperandHelper::IsImmediate(operand_2)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_2);
  }
  switch (OperandHelper::Value(operand_1)->value) {
    case Util::EncodeBase36<int>("nz"):
      return Write::MachineCode(base_.data_, 0xC2, base_.errors_, operand_2, 2);
    case Util::EncodeBase36<int>("z"):
      return Write::MachineCode(base_.data_, 0xCA, base_.errors_, operand_2, 2);
    case Util::EncodeBase36<int>("nc"):
      return Write::MachineCode(base_.data_, 0xD2, base_.errors_, operand_2, 2);
    case Util::EncodeBase36<int>("c"):
      return Write::MachineCode(base_.data_, 0xDA, base_.errors_, operand_2, 2);
    default:
      return ErrMaker::InvalidFlag(base_.errors_, operand_1);
  }
}

void GeneratorImpl::OP_jr(const Node::Pointer& opcode) {
  const auto operand_count{OpcodeHelper::GetOperandCount(opcode)};
  if (operand_count == 0) {
    return ErrMaker::NeedOperand(base_.errors_, opcode);
  } else if (operand_count == 1) {
    return OP_jr_xx(opcode->lhs);
  } else if (operand_count == 2) {
    return OP_jr_xx_xx(opcode->lhs, opcode->lhs->lhs);
  } else {
    return ErrMaker::TooManyOperand(base_.errors_, opcode);
  }
}

void GeneratorImpl::OP_jr_xx(const Node::Pointer& operand) {
  if (OperandHelper::IsImmediate(operand)) {
    return Write::MachineCode(base_.data_, 0x18, base_.errors_, operand, 1);
  } else {
    return ErrMaker::NeedOperand(base_.errors_, operand);
  }
}

void GeneratorImpl::OP_jr_xx_xx(const Node::Pointer& operand_1,
                                const Node::Pointer& operand_2) {
  if (!OperandHelper::IsImmediate(operand_1) ||
      !OperandHelper::IsRegister(operand_1)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_1);
  } else if (!OperandHelper::IsImmediate(operand_2)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_2);
  }
  switch (OperandHelper::Value(operand_1)->value) {
    case Util::EncodeBase36<int>("nz"):
      return Write::MachineCode(base_.data_, 0x20, base_.errors_, operand_2, 1);
    case Util::EncodeBase36<int>("z"):
      return Write::MachineCode(base_.data_, 0x28, base_.errors_, operand_2, 1);
    case Util::EncodeBase36<int>("nc"):
      return Write::MachineCode(base_.data_, 0x30, base_.errors_, operand_2, 1);
    case Util::EncodeBase36<int>("c"):
      return Write::MachineCode(base_.data_, 0x38, base_.errors_, operand_2, 1);
    default:
      return ErrMaker::InvalidFlag(base_.errors_, operand_1);
  }
}

void GeneratorImpl::OP_call(const Node::Pointer& opcode) {
  const auto operand_count{OpcodeHelper::GetOperandCount(opcode)};
  if (operand_count == 0) {
    return ErrMaker::NeedOperand(base_.errors_, opcode);
  } else if (operand_count == 1) {
    return OP_call_xx(opcode->lhs);
  } else if (operand_count == 2) {
    return OP_call_xx_xx(opcode->lhs, opcode->lhs->lhs);
  } else {
    return ErrMaker::TooManyOperand(base_.errors_, opcode);
  }
}

void GeneratorImpl::OP_call_xx(const Node::Pointer& operand) {
  if (OperandHelper::IsImmediate(operand)) {
    return Write::MachineCode(base_.data_, 0xCD, base_.errors_, operand, 2);
  } else {
    return ErrMaker::NeedOperand(base_.errors_, operand);
  }
}

void GeneratorImpl::OP_call_xx_xx(const Node::Pointer& operand_1,
                                  const Node::Pointer& operand_2) {
  if (!OperandHelper::IsImmediate(operand_1) ||
      !OperandHelper::IsRegister(operand_1)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_1);
  } else if (!OperandHelper::IsImmediate(operand_2)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_2);
  }
  switch (OperandHelper::Value(operand_1)->value) {
    case Util::EncodeBase36<int>("nz"):
      return Write::MachineCode(base_.data_, 0xC4, base_.errors_, operand_2, 2);
    case Util::EncodeBase36<int>("z"):
      return Write::MachineCode(base_.data_, 0xCC, base_.errors_, operand_2, 2);
    case Util::EncodeBase36<int>("nc"):
      return Write::MachineCode(base_.data_, 0xD4, base_.errors_, operand_2, 2);
    case Util::EncodeBase36<int>("c"):
      return Write::MachineCode(base_.data_, 0xDC, base_.errors_, operand_2, 2);
    default:
      return ErrMaker::InvalidFlag(base_.errors_, operand_1);
  }
}

void GeneratorImpl::OP_ret(const Node::Pointer& opcode) {
  const auto operand_count{OpcodeHelper::GetOperandCount(opcode)};
  if (operand_count == 0) {
    return Write::MachineCode(base_.data_, 0xC9);
  } else if (operand_count == 1) {
    return OP_ret_xx(opcode->lhs);
  } else {
    return ErrMaker::TooManyOperand(base_.errors_, opcode);
  }
}

void GeneratorImpl::OP_ret_xx(const Node::Pointer& operand) {
  if (!OperandHelper::IsImmediate(operand) ||
      !OperandHelper::IsRegister(operand)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand);
  }
  switch (OperandHelper::Value(operand)->value) {
    case Util::EncodeBase36<int>("nz"):
      return Write::MachineCode(base_.data_, 0xC0);
    case Util::EncodeBase36<int>("z"):
      return Write::MachineCode(base_.data_, 0xC8);
    case Util::EncodeBase36<int>("nc"):
      return Write::MachineCode(base_.data_, 0xD0);
    case Util::EncodeBase36<int>("c"):
      return Write::MachineCode(base_.data_, 0xD8);
    default:
      return ErrMaker::InvalidFlag(base_.errors_, operand);
  }
}

void GeneratorImpl::OP_rst(const Node::Pointer& opcode) {
  constexpr int kOperandCount{1};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  }
  const auto operand{opcode->lhs};
  if (OperandHelper::IsImmediate(operand) && OperandHelper::IsNumber(operand)) {
    const auto op_v{OperandHelper::Value(operand)->value};
    if (op_v >= 0x00 && op_v <= 0x38 && op_v % 8 == 0) {
      return Write::MachineCode(base_.data_, 0xC7 + op_v);
    } else {
      return ErrMaker::InvalidOperand_rst(base_.errors_, operand);
    }
  } else {
    return ErrMaker::InvalidOperand_rst(base_.errors_, operand);
  }
}

void GeneratorImpl::OP_db(const Node::Pointer& opcode) {
  if (OpcodeHelper::GetOperandCount(opcode) == 0) {
    return ErrMaker::NeedOperand(base_.errors_, opcode);
  }
  auto operand{opcode->lhs};
  while (operand != nullptr) {
    Write::Operand(base_.data_, base_.errors_, operand, 1);
    operand = operand->lhs;
  }
}

void GeneratorImpl::OP_ds(const Node::Pointer& opcode) {
  constexpr int kOperandCount{1};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  }
  const auto operand{opcode->lhs};
  if (!OperandHelper::IsImmediate(operand) ||
      !OperandHelper::IsNumber(operand)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand);
  }
  const auto len{OperandHelper::Value(operand)->value};
  for (int i = 0; i < len; ++i) {
    Write::Raw(base_.data_, 0x00, 1);
  }
}

void GeneratorImpl::OP_dp(const Node::Pointer& opcode) {
  constexpr int kOperandCount{1};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  }
  const auto operand{opcode->lhs};
  if (!OperandHelper::IsImmediate(operand) ||
      !OperandHelper::IsNumber(operand)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand);
  }
  const int current_pos{static_cast<int>(base_.data_.os.tellp())};
  const int target_pos{OperandHelper::Value(operand)->value};
  if (target_pos < current_pos) {
    return ErrMaker::PaddingOverflow(base_.errors_, operand, current_pos);
  }
  for (int i = current_pos; i < target_pos; ++i) {
    Write::Raw(base_.data_, 0x00, 1);
  }
}

void GeneratorImpl::OP_def(const Node::Pointer& opcode) {
  constexpr int kOperandCount{2};
  if (OpcodeHelper::GetOperandCount(opcode) != kOperandCount) {
    return ErrMaker::NumOfOperand(base_.errors_, opcode, kOperandCount);
  }
  const auto operand_1{opcode->lhs};
  const auto operand_2{opcode->lhs->lhs};
  if (!OperandHelper::IsImmediate(operand_1) ||
      !OperandHelper::IsIdent(operand_1)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_1);
  }
  if (!OperandHelper::IsImmediate(operand_2)) {
    return ErrMaker::InvalidOperand(base_.errors_, operand_2);
  }
  std::string name{OperandHelper::Value(operand_1)->token.str};
  if (SymbolHelper::FindName(base_.data_, name)) {
    return ErrMaker::DuplicateName(base_.errors_, operand_1);
  }
  if (OperandHelper::IsValue(operand_2)) {
    auto value_node{OperandHelper::Value(operand_2)};
    if (name == value_node->token.str) {
      return ErrMaker::CircularReference(base_.errors_, value_node);
    } else {
      return SymbolHelper::StoreDefine(base_.data_, std::move(name),
                                       std::move(value_node));
    }
  } else if (OperandHelper::IsFormula(operand_2)) {
    std::unordered_set<std::string> ng{name};
    auto node{
        Calculator::Run(base_.data_, ng, OperandHelper::Formula(operand_2))};
    if (ng.count("!") != 0) {
      return ErrMaker::CircularReference(base_.errors_, node);
    } else {
      return SymbolHelper::StoreDefine(base_.data_, std::move(name),
                                       std::move(node));
    }
  } else {
    return ErrMaker::InvalidOperand(base_.errors_, operand_2);
  }
}

}  // namespace AssemblerImpl

}  // namespace GBDev