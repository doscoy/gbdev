#pragma once

#include "tokenizer.hpp"

namespace GBDev {

namespace AssemblerImpl {

class TokenizerImpl {
  // boilerplate {
  enum class Kind { UNKNOW, EOL, PREHEX, PREBIN, NUMBER, SYMBOL, STRING };
  using Base = TokenizerBase<TokenizerImpl>;
  friend Base;
  TokenizerImpl(Base& base) : base_(base) {}
  void Tokenize(const std::string& src);
  Base& base_;
  // } boilerplate

  using Token = Token<Kind>;

  class CursorMgr;

  template <class Lambda>
  bool StoreHelper(const Lambda& func, const Token::Kind kind,
                   CursorMgr& cursor);

  bool CheckSkipCharacter(CursorMgr& cursor);

  bool CheckComment(CursorMgr& cursor);

  bool CheckEOLToken(CursorMgr& cursor);

  bool CheckNumberToken(CursorMgr& cursor);

  bool CheckDoubleSymbolToken(CursorMgr& cursor);

  bool CheckSingleSymbolToken(CursorMgr& cursor);

  bool CheckStringToken(CursorMgr& cursor);

  void HandlingInvalidCharacter(CursorMgr& cursor);
};
using Tokenizer = TokenizerBase<TokenizerImpl>;

}

}